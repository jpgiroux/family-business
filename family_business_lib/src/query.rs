use rocket::{FromForm, FromFormField};
use std::convert::From;

#[derive(Debug, PartialEq, Eq, Clone, FromForm)]
pub struct Pagination {
    pub page: usize,
    pub size: PageSize,
}

impl Pagination {
    pub fn default() -> Pagination {
        Pagination {
            page: 0,
            size: PageSize::TwentyFive,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, FromFormField)]
pub enum PageSize {
    TwentyFive,
    Fifty,
    Invalid,
}

#[derive(Debug, PartialEq, Eq, Clone, FromFormField)]
pub enum Ordering {
    Asc,
    Desc,
}
