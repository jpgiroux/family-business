pub mod bills;
pub mod query;
pub mod security;
pub mod todo;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct FamilyMember {
    pub id: i32,
    pub user_id: Option<i32>,
    pub name: String,
    pub is_parent: bool,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewFamilyMember {
    pub name: String,
    pub user_id: Option<i32>,
    pub is_parent: bool,
}
