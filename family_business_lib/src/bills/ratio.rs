use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Ratio {
    pub parent_id: i32,
    pub value: f32,
}

#[derive(Debug, PartialEq, Eq)]
pub enum RatioError {
    ValueLowerThanZero,
    ValueGreaterThanOne,
    ValuesDoesNotSumUpToOne,
}

pub fn validate(ratios: &[Ratio]) -> Result<(), RatioError> {
    if ratios.iter().any(|ratio| ratio.value < 0.0) {
        return Err(RatioError::ValueLowerThanZero);
    }

    if ratios.iter().any(|ratio| ratio.value > 1.0) {
        return Err(RatioError::ValueGreaterThanOne);
    }

    if ratios.iter().fold(0.0, |acc, ratio| acc + ratio.value) != 1.0 {
        return Err(RatioError::ValuesDoesNotSumUpToOne);
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    fn build_ratio(value: f32) -> Ratio {
        Ratio {
            parent_id: 0,
            value,
        }
    }

    #[test]
    fn when_one_ratio_lower_than_zero_then_error() {
        let results = validate(&vec![build_ratio(-1.0)]);
        assert_eq!(results, Err(RatioError::ValueLowerThanZero))
    }

    #[test]
    fn when_one_ratio_greater_than_one_then_error() {
        let results = validate(&vec![build_ratio(1.1)]);
        assert_eq!(results, Err(RatioError::ValueGreaterThanOne))
    }

    #[test]
    fn when_empty_then_error() {
        let results = validate(&vec![]);
        assert_eq!(results, Err(RatioError::ValuesDoesNotSumUpToOne))
    }

    #[test]
    fn when_low_values_then_error() {
        let results = validate(&vec![build_ratio(0.05), build_ratio(0.05)]);
        assert_eq!(results, Err(RatioError::ValuesDoesNotSumUpToOne))
    }

    #[test]
    fn when_big_values_then_error() {
        let results = validate(&vec![build_ratio(0.8), build_ratio(0.8)]);
        assert_eq!(results, Err(RatioError::ValuesDoesNotSumUpToOne))
    }

    #[test]
    fn when_fifty_fifty_split_then_ok() {
        let results = validate(&vec![build_ratio(0.5), build_ratio(0.5)]);
        assert_eq!(results, Ok(()))
    }

    #[test]
    fn when_sixty_forty_split_then_ok() {
        let results = validate(&vec![build_ratio(0.6), build_ratio(0.4)]);
        assert_eq!(results, Ok(()))
    }
}
