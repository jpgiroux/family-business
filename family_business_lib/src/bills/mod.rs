pub mod debt;
pub mod payee;
pub mod ratio;

use crate::{query, query::Ordering};
use chrono::NaiveDate;

use crate::bills::payee::Payee;
use crate::bills::ratio::Ratio;
use rocket::{FromForm, FromFormField};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewBill {
    pub payer_id: i32,
    pub amount: f32,
    pub payee: Payee,
    pub ratios: Vec<Ratio>,
    pub description: Option<String>,
    pub date: Option<NaiveDate>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Bill {
    pub id: i32,
    pub payer_id: i32,
    pub amount: f32,
    pub payee: Payee,
    pub ratios: Vec<Ratio>,
    pub description: Option<String>,
    pub date: Option<NaiveDate>,
    pub resolve_date: Option<NaiveDate>,
}

//todo add a ResolveBill, change bill to remove the resolve date.

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct BillsResolution {
    pub ids: Vec<i32>,
    pub resolve_date: NaiveDate,
}

pub fn validate(resolution: &BillsResolution, bills: &[Bill]) -> Result<(), BillsResolutionError> {
    let any_not_found = resolution
        .ids
        .iter()
        .any(|id| !bills.iter().any(|bill| bill.id == *id));

    if any_not_found {
        Err(BillsResolutionError::InvalidId)
    } else {
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum BillsResolutionError {
    InvalidId,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum BillError {
    AmountNegativeOrZero,
    RatiosDoesNotSumUpToOne,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct BillResolvedQueryResult {
    pub bills: Vec<Bill>,
    pub total_elements: i64,
}

#[derive(Debug, FromFormField)]
pub enum BillSortField {
    PayerId,
    Amount,
    Description,
    Date,
    ResolveDate,
}

#[derive(Debug, FromForm)]
pub struct BillSortExpression {
    pub field: BillSortField,
    pub ordering: query::Ordering,
}

impl BillSortExpression {
    pub fn default() -> BillSortExpression {
        BillSortExpression {
            field: BillSortField::Date,
            ordering: Ordering::Desc,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_bills() -> Vec<Bill> {
        vec![
            Bill {
                id: 1,
                payer_id: 0,
                amount: 0.0,
                payee: Payee::All,
                ratios: vec![],
                description: None,
                date: None,
                resolve_date: None,
            },
            Bill {
                id: 2,
                payer_id: 0,
                amount: 0.0,
                payee: Payee::All,
                ratios: vec![],
                description: None,
                date: None,
                resolve_date: None,
            },
        ]
    }

    #[test]
    fn given_a_bill_resolution_with_one_id_unknown_when_validating_then_err() {
        let resolution = BillsResolution {
            ids: vec![1, 2, 3], //contain 1 aditionnal/unknown id
            resolve_date: NaiveDate::from_ymd(2022, 4, 19),
        };

        let bills = get_bills();

        let result = validate(&resolution, &bills);

        assert_eq!(result, Err(BillsResolutionError::InvalidId))
    }

    #[test]
    fn given_a_valid_bill_resolution_when_validating_then_ok() {
        let resolution = BillsResolution {
            ids: vec![1, 2],
            resolve_date: NaiveDate::from_ymd(2022, 4, 19),
        };

        let bills = get_bills();

        let result = validate(&resolution, &bills);

        assert_eq!(result, Ok(()))
    }
}
