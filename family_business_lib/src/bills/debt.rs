use crate::bills::{payee::Payee, Bill};
use crate::FamilyMember;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::iter::FromIterator;

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Debt {
    pub owed_by: FamilyMember,
    pub owed_to: FamilyMember,
    pub owed: f32,
}

pub fn calculate_debt(family: &[FamilyMember], bills: &[Bill]) -> Vec<Debt> {
    let mut debts = HashMap::<String, Debt>::new();

    let family_map = HashMap::from_iter(family.iter().map(|member| (member.id, member)));
    for bill in bills {
        match bill.payee {
            Payee::All => {
                calculate_debt_for_parents(bill, &mut debts, &family_map);
            }
            Payee::Kids => {
                calculate_debt_for_kids(bill, &mut debts, &family_map);
                calculate_debt_for_parents(bill, &mut debts, &family_map);
            }
        }
    }
    debts.values().cloned().collect()
}

fn calculate_debt_for_parents(
    bill: &Bill,
    debts: &mut HashMap<String, Debt>,
    family: &HashMap<i32, &FamilyMember>,
) {
    let owed_to: &FamilyMember = family.get(&bill.payer_id).expect("family member not found");

    for payee_ratio in bill
        .ratios
        .iter()
        .filter(|ratio| ratio.parent_id != bill.payer_id)
    {
        let owed_by: &FamilyMember = family
            .get(&payee_ratio.parent_id)
            .expect("family member not found");

        let debt = debts
            .entry(format!(
                "{}->{}",
                owed_by.name.clone(),
                owed_to.name.clone()
            ))
            .or_insert(Debt {
                owed_by: owed_by.clone(),
                owed_to: owed_to.clone(),
                owed: 0.0,
            });

        debt.owed += bill.amount * payee_ratio.value;
    }
}

fn calculate_debt_for_kids(
    bill: &Bill,
    debts: &mut HashMap<String, Debt>,
    family: &HashMap<i32, &FamilyMember>,
) {
    let kids = family
        .values()
        .filter(|member| !member.is_parent)
        .copied()
        .collect::<Vec<_>>();
    let nb_kid = kids.len();

    let bill_split = bill.amount / (nb_kid as f32);

    for kid in kids.into_iter() {
        for ratio in bill.ratios.iter() {
            let parent_ratio: &FamilyMember = family
                .get(&ratio.parent_id)
                .expect("family member not found");

            let debt = debts
                .entry(format!("{}->{}", kid.name, parent_ratio.name))
                .or_insert(Debt {
                    owed_by: kid.clone(),
                    owed_to: parent_ratio.clone(),
                    owed: 0.0,
                });

            debt.owed += bill_split * ratio.value;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::bills::ratio::Ratio;

    const MEMBER_1: i32 = 1;
    const MEMBER_2: i32 = 2;
    const MEMBER_3: i32 = 3;
    const MEMBER_4: i32 = 4;

    fn get_family_test() -> [FamilyMember; 4] {
        [
            FamilyMember {
                id: MEMBER_1,
                name: "JP".to_string(),
                user_id: None,
                is_parent: true,
            },
            FamilyMember {
                id: MEMBER_2,
                name: "Clau".to_string(),
                user_id: None,
                is_parent: true,
            },
            FamilyMember {
                id: MEMBER_3,
                name: "Olie".to_string(),
                user_id: None,
                is_parent: false,
            },
            FamilyMember {
                id: MEMBER_4,
                name: "Ludo".to_string(),
                user_id: None,
                is_parent: false,
            },
        ]
    }

    fn build_test_bill(
        payer_id: i32,
        amount: f32,
        payee: Payee,
        ratios: Option<(f32, f32)>,
    ) -> Bill {
        let ratios = ratios.unwrap_or((0.5, 0.5));
        let ratios = vec![
            Ratio {
                parent_id: MEMBER_1,
                value: ratios.0,
            },
            Ratio {
                parent_id: MEMBER_2,
                value: ratios.1,
            },
        ];

        Bill {
            id: 0, //not important
            payer_id,
            amount,
            payee,
            ratios,
            description: None,
            date: None,
            resolve_date: None,
        }
    }

    #[test]
    fn test_one_bill() {
        let bills: [Bill; 1] = [build_test_bill(MEMBER_1, 10.0, Payee::All, None)];

        let family = get_family_test();

        let results = calculate_debt(&family, &bills);

        assert_eq!(
            results,
            vec!(Debt {
                owed_by: family[1].clone(),
                owed_to: family[0].clone(),
                owed: 5.0
            })
        );
    }

    #[test]
    fn test_one_bill_ratio_1_0() {
        let bills: [Bill; 1] = [build_test_bill(
            MEMBER_1,
            10.0,
            Payee::All,
            Some((0.0, 1.0)),
        )];

        let family = get_family_test();

        let results = calculate_debt(&family, &bills);

        assert_eq!(
            results,
            vec!(Debt {
                owed_by: family[1].clone(),
                owed_to: family[0].clone(),
                owed: 10.0
            })
        );
    }

    #[test]
    fn test_one_bill_ratio_0_1() {
        let bills: [Bill; 1] = [build_test_bill(
            MEMBER_1,
            10.0,
            Payee::All,
            Some((1.0, 0.0)),
        )];

        let family = get_family_test();

        let results = calculate_debt(&family, &bills);

        assert_eq!(
            results,
            vec!(Debt {
                owed_by: family[1].clone(),
                owed_to: family[0].clone(),
                owed: 0.0
            })
        );
    }

    #[test]
    fn test_multiple_bills() {
        let bills: [Bill; 6] = [
            build_test_bill(MEMBER_1, 10.0, Payee::All, None),
            build_test_bill(MEMBER_1, 3.33, Payee::All, None),
            build_test_bill(MEMBER_1, 2.45, Payee::All, None),
            build_test_bill(MEMBER_2, 11.11, Payee::All, None),
            build_test_bill(MEMBER_2, 5.55, Payee::All, None),
            build_test_bill(MEMBER_2, 3.99, Payee::All, None),
        ];

        let family = get_family_test();

        let results = calculate_debt(&family, &bills);

        assert_eq!(results.len(), 2);
        assert!(results.contains(&Debt {
            owed_by: family[0].clone(),
            owed_to: family[1].clone(),
            owed: 10.325
        }));
        assert!(results.contains(&Debt {
            owed_by: family[1].clone(),
            owed_to: family[0].clone(),
            owed: 7.89
        }));
    }

    #[test]
    fn test_one_bill_kids() {
        let bills: [Bill; 1] = [build_test_bill(MEMBER_1, 10.0, Payee::Kids, None)];

        let family = get_family_test();

        let results = calculate_debt(&family, &bills);

        assert_eq!(results.len(), 5);

        assert!(results.contains(&Debt {
            owed_by: family[3].clone(),
            owed_to: family[1].clone(),
            owed: 2.5
        }));

        assert!(results.contains(&Debt {
            owed_by: family[2].clone(),
            owed_to: family[1].clone(),
            owed: 2.5
        }));

        assert!(results.contains(&Debt {
            owed_by: family[3].clone(),
            owed_to: family[0].clone(),
            owed: 2.5
        }));

        assert!(results.contains(&Debt {
            owed_by: family[2].clone(),
            owed_to: family[0].clone(),
            owed: 2.5
        }));

        assert!(results.contains(&Debt {
            owed_by: family[1].clone(),
            owed_to: family[0].clone(),
            owed: 5.0
        }));
    }
}
