use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Formatter;
use std::str::FromStr;

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq, sqlx::Type)]
#[sqlx(rename_all = "UPPERCASE")]
pub enum Payee {
    All,
    Kids,
}

impl fmt::Display for Payee {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Payee::All => write!(f, "ALL"),
            Payee::Kids => write!(f, "KIDS"),
        }
    }
}

impl FromStr for Payee {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s == "ALL" {
            Ok(Payee::All)
        } else if s == "KIDS" {
            Ok(Payee::Kids)
        } else {
            Err(())
        }
    }
}
