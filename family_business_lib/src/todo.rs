use crate::FamilyMember;
use chrono::{Local, NaiveDate};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Formatter;
use uuid::Uuid;

#[derive(Debug, Deserialize, Clone)]
pub struct AddToDoRequest {
    name: String,
    description: String,
    assignee: Option<i32>,
    due_date: Option<NaiveDate>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct EditToDoRequest {
    pub id: uuid::Uuid,
    name: String,
    description: String,
    assignee: Option<i32>,
    due_date: Option<NaiveDate>,
}

#[derive(Debug, PartialEq, Eq, Serialize, Clone)]
pub struct ToDo {
    pub id: uuid::Uuid,
    pub name: String,
    pub description: String,
    pub assignee: Option<FamilyMember>,
    pub due_date: Option<NaiveDate>,
    pub status: Status,
}

#[derive(Debug, PartialEq, Eq)]
pub enum ToDoError {
    AssigneeNotFound(i32),
    DueDatePast(NaiveDate),
    EmptyName,
    EmptyDesc,
    WrongId,
}

impl fmt::Display for ToDoError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            ToDoError::AssigneeNotFound(id) => write!(f, "Assignee '{}' not found", id),
            ToDoError::DueDatePast(date) => write!(f, "Due date '{}' is in the past", date),
            ToDoError::EmptyName => write!(f, "Name is empty or white"),
            ToDoError::EmptyDesc => write!(f, "Description is empty or white"),
            ToDoError::WrongId => write!(f, "id mismatch edited"),
        }
    }
}

impl ToDo {
    pub fn from_add_request(
        request: &AddToDoRequest,
        family: &[FamilyMember],
    ) -> Result<ToDo, ToDoError> {
        let assignee = ToDo::validate_assignee_found(&request.assignee, family)?;
        let due_date = ToDo::validate_due_date_not_past(&request.due_date)?;
        let trim_name = ToDo::trim_validate_name(&request.name)?;
        let trim_desc = ToDo::trim_validate_desc(&request.description)?;

        Ok(ToDo {
            id: Uuid::new_v4(),
            name: trim_name,
            description: trim_desc,
            assignee,
            due_date,
            status: Status::Open,
        })
    }

    pub fn apply_edit_request(
        &mut self,
        request: &EditToDoRequest,
        family: &[FamilyMember],
    ) -> Result<(), ToDoError> {
        if self.id != request.id {
            return Err(ToDoError::WrongId);
        }

        self.assignee = ToDo::validate_assignee_found(&request.assignee, family)?;
        self.due_date = ToDo::validate_due_date_not_past(&request.due_date)?;
        self.name = ToDo::trim_validate_name(&request.name)?;
        self.description = ToDo::trim_validate_desc(&request.description)?;

        Ok(())
    }

    pub fn close(&mut self) {
        self.status = Status::Close;
    }

    fn validate_assignee_found(
        assignee: &Option<i32>,
        family: &[FamilyMember],
    ) -> Result<Option<FamilyMember>, ToDoError> {
        match assignee {
            Some(id) => Ok(Some(
                family
                    .iter()
                    .find(|member| member.id == *id)
                    .ok_or(ToDoError::AssigneeNotFound(*id))?
                    .clone(),
            )),
            None => Ok(None),
        }
    }

    fn validate_due_date_not_past(
        due_date: &Option<NaiveDate>,
    ) -> Result<Option<NaiveDate>, ToDoError> {
        match *due_date {
            Some(date) => match date < Local::today().naive_local() {
                true => Err(ToDoError::DueDatePast(date)),
                _ => Ok(Some(date)),
            },
            None => Ok(None),
        }
    }

    fn trim_validate_name(name: &str) -> Result<String, ToDoError> {
        let trim_name = name.trim();

        match trim_name.is_empty() {
            true => Err(ToDoError::EmptyName),
            _ => Ok(trim_name.to_string()),
        }
    }

    fn trim_validate_desc(desc: &str) -> Result<String, ToDoError> {
        let trim_desc = desc.trim();
        match trim_desc.is_empty() {
            true => Err(ToDoError::EmptyDesc),
            _ => Ok(trim_desc.to_string()),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Copy, Clone, sqlx::Type, Serialize, Deserialize)]
#[sqlx(rename_all = "UPPERCASE")]
pub enum Status {
    Open,
    Close,
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_family() -> Vec<FamilyMember> {
        vec![
            FamilyMember {
                id: 1,
                user_id: None,
                name: "JP".to_string(),
                is_parent: true,
            },
            FamilyMember {
                id: 2,
                user_id: None,
                name: "Clau".to_string(),
                is_parent: true,
            },
        ]
    }

    #[test]
    pub fn given_assignee_not_in_family_when_creating_from_request_then_error() {
        let bad_request = AddToDoRequest {
            name: "a name".to_string(),
            description: "a desc".to_string(),
            assignee: Some(0),
            due_date: None,
        };

        assert_eq!(
            ToDo::from_add_request(&bad_request, &get_family()),
            Err(ToDoError::AssigneeNotFound(0))
        )
    }

    #[test]
    pub fn given_past_due_date_when_creating_from_request_then_error() {
        let bad_request = AddToDoRequest {
            name: "a name".to_string(),
            description: "a desc".to_string(),
            assignee: None,
            due_date: Some(NaiveDate::from_ymd(2022, 05, 23)),
        };

        assert_eq!(
            ToDo::from_add_request(&bad_request, &get_family()),
            Err(ToDoError::DueDatePast(NaiveDate::from_ymd(2022, 05, 23)))
        )
    }

    #[test]
    pub fn given_assigne_id_when_creating_from_request_then_assigne_from_family() {
        let request = AddToDoRequest {
            name: "a name".to_string(),
            description: "a desc".to_string(),
            assignee: Some(1),
            due_date: None,
        };

        let family = get_family();

        assert_eq!(
            ToDo::from_add_request(&request, &family).unwrap().assignee,
            Some(family[0].clone())
        )
    }

    #[test]
    pub fn given_request_when_creating_from_request_then_status_is_open() {
        let request = AddToDoRequest {
            name: "a name".to_string(),
            description: "a desc".to_string(),
            assignee: None,
            due_date: None,
        };

        assert_eq!(
            ToDo::from_add_request(&request, &get_family())
                .unwrap()
                .status,
            Status::Open
        )
    }

    #[test]
    pub fn given_description_when_creating_from_request_then_description_is_same() {
        let request = AddToDoRequest {
            name: "a name".to_string(),
            description: "a super description".to_string(),
            assignee: None,
            due_date: None,
        };

        assert_eq!(
            ToDo::from_add_request(&request, &get_family())
                .unwrap()
                .description,
            "a super description".to_string()
        )
    }

    #[test]
    pub fn given_name_when_creating_from_request_then_name_is_same() {
        let request = AddToDoRequest {
            name: "name me this".to_string(),
            description: "a desc".to_string(),
            assignee: None,
            due_date: None,
        };

        assert_eq!(
            ToDo::from_add_request(&request, &get_family())
                .unwrap()
                .name,
            "name me this".to_string()
        )
    }

    #[test]
    pub fn given_empty_name_when_creating_from_request_then_err() {
        let bad_request = AddToDoRequest {
            name: "".to_string(),
            description: "a desc".to_string(),
            assignee: None,
            due_date: None,
        };

        assert_eq!(
            ToDo::from_add_request(&bad_request, &get_family()),
            Err(ToDoError::EmptyName)
        )
    }

    #[test]
    pub fn given_white_name_when_creating_from_request_then_err() {
        let bad_request = AddToDoRequest {
            name: "  ".to_string(),
            description: "a desc".to_string(),
            assignee: None,
            due_date: None,
        };

        assert_eq!(
            ToDo::from_add_request(&bad_request, &get_family()),
            Err(ToDoError::EmptyName)
        )
    }

    #[test]
    pub fn given_empty_description_when_creating_from_request_then_err() {
        let bad_request = AddToDoRequest {
            name: "a name".to_string(),
            description: "".to_string(),
            assignee: None,
            due_date: None,
        };

        assert_eq!(
            ToDo::from_add_request(&bad_request, &get_family()),
            Err(ToDoError::EmptyDesc)
        )
    }

    #[test]
    pub fn given_white_description_when_creating_from_request_then_err() {
        let bad_request = AddToDoRequest {
            name: "a name".to_string(),
            description: "    ".to_string(),
            assignee: None,
            due_date: None,
        };

        assert_eq!(
            ToDo::from_add_request(&bad_request, &get_family()),
            Err(ToDoError::EmptyDesc)
        )
    }

    #[test]
    pub fn given_an_opened_todo_when_closing_then_todo_is_closed() {
        let mut todo = ToDo {
            id: Default::default(),
            name: "a name".to_string(),
            description: "   ".to_string(),
            assignee: None,
            due_date: None,
            status: Status::Open,
        };

        todo.close();

        assert_eq!(todo.status, Status::Close);
    }
}
