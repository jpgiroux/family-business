export const environment = {
  production: true,
  useMock: false,
  apiUrl: "https://fopapp.xyz/api"
};
