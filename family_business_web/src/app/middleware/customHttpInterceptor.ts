import { HttpInterceptor, HttpErrorResponse, HttpEvent, HttpResponse, HttpRequest, HttpHandler } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, of, throwError } from "rxjs";
import { tap, catchError, finalize } from "rxjs/operators";
import { AuthentificationService } from "../services/authentification.service";

@Injectable({
  providedIn: 'root'
})
export class CustomHttpInterceptor implements HttpInterceptor {
  constructor(
    // Service de routing
    private readonly _router: Router,

    private readonly _authService: AuthentificationService

    // Service gérant l'animation de chargement (spinner par exemple)
    //private readonly _loaderService: LoaderService
  ) { }
  // Créer une réponse HTTP avec un statut particulier à partir d'une erreur
  private createHttpResponseFromHttpErrorResponse(httpErrorResponse: HttpErrorResponse, status: number = 0): Observable<HttpEvent<any>> {
    return of(
      new HttpResponse({
        headers: httpErrorResponse.headers,
        status: status ?? httpErrorResponse.status,
        statusText: httpErrorResponse.statusText,
        url: httpErrorResponse.url ?? undefined,
        body: httpErrorResponse.error
      })
    );
  }
  // Le point d'entrée de l'intercepteur
  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Ajout des headers dont le Content-Type
    const newRequest = request.clone({
      //url: request.url.replace('http://', 'https://'),
      setHeaders: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this._authService.getToken()}`
      }
    });

    // Si la requête n'a pas besoin d'être mise en cache (c'est le cas des requêtes non GET ou des requêtes GET que nous ne voulons pas mettre en cache), nous gérons la requête
    if (newRequest.method !== 'GET') {
      return this.requestHandler(newRequest, next);
    }
    // Gestion du cas des requêtes parallèles sur la même URL. Nous renvoyons la requête en cours sinon celle mise en cache
    return this.requestHandler(newRequest, next);
  }
  // Gestion de la requête
  private requestHandler(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const startRequestTime = Date.now();
    let isRequestInSucces = true;
    // Affichons le loader
    //this._loaderService.show();
    return next.handle(request).pipe(
      catchError(
        // Dans le cas d'une réponse en erreur
        (httpErrorResponse: HttpErrorResponse): Observable<HttpEvent<any>> => {
          isRequestInSucces = false;
          let errorMessage: string = "";
          if (httpErrorResponse.error instanceof ErrorEvent) {
            // Erreur côté client
            errorMessage = httpErrorResponse.error.message;
          } else {
            // Erreur côté serveur
            switch (httpErrorResponse.status) {
              case 401:
                console.error('(CustomHttpInterceptor)', 'Unauthorized request');
                this._router.navigate(['/login']);
                break;
              case 403:
                console.error('(CustomHttpInterceptor)', 'Forbidden request');
                this._router.navigate(['/error/forbidden']);
                break;
              case 404:
                return this.createHttpResponseFromHttpErrorResponse(httpErrorResponse);
              default:
                errorMessage = httpErrorResponse.message || httpErrorResponse.statusText;
            }
          }

          // Retour de l'erreur à l'appelant pour qu'il la gère à sa manière
          return throwError(errorMessage);
        }
      ),
      finalize(() => {
        // Cachons le loader
        //this._loaderService.hide();
        // Le petit plus : nous affichons dans la console les informations de la requête et son temps d'exécution
        const elapsedRequestTime = Date.now() - startRequestTime;
        console.log(
          `HTTP ${request.method} ${request.url} ${request.body ? 'BODY: ' + JSON.stringify(request.body) : ''} (${isRequestInSucces ? 'SUCCESS' : 'ECHEC'
          } ${elapsedRequestTime}ms)`
        );
      })
    );
  }
}
