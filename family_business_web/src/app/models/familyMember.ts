import { BaseModel } from "./baseModel";

export class FamilyMember implements BaseModel
{
    constructor(
        public id: number = 0,
        public name: string = "",
        public is_parent: boolean = false,
    )
    { }
}
