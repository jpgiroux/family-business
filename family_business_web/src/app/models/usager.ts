import { BaseModel } from "./baseModel";

export class Usager implements BaseModel {

  constructor(
    public id: number = 0,
    public nom_usager: string = "",
    public mot_passe: string = "",

    public nom: string = "",
    public prenom: string = "",
    public id_role: number = 0,
    public id_compagnie: number = 0,

    public confirm_mot_passe: string = "") {
  }
}

