import { FamilyMember } from './familyMember';
import { BaseModel } from "./baseModel";

export class Debt implements BaseModel
{
    constructor(
        public owedBy: FamilyMember = new FamilyMember(),
        public owedTo: FamilyMember = new FamilyMember(),
        public owed: number = 0
    )
    { }
}
