import { Ratios } from './ratios';
import { BaseModel } from "./baseModel";

export class Bills implements BaseModel
{
    constructor(
        public id: number = 0,
        public payer_id: number = 0,
        public amount: number = 0,
        public payee: string = "All",
        public ratios: Ratios[] = new Array<Ratios>(),
        public description: string = "",
        public date: string = "",
        public resolve_date: string = "",
    )
    { }
}

export enum BillsSortField
{
  PAYER_ID = "PayerId",
  AMOUNT = "Amount",
  DESCRIPTION = "Description",
  DATE = "Date",
  RESOLVE_DATE = "ResolveDate"
}

