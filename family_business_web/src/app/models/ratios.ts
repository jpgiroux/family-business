import { BaseModel } from "./baseModel";

export class Ratios implements BaseModel
{
    constructor(
        public parent_id: number = 0,
        public value: number = 0,
    )
    { }
}
