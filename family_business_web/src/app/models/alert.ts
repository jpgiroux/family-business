export class Alert {
  id: string;
  type: AlertType;
  message: string;
  autoClose: boolean;
  keepAfterRouteChange: boolean;
  fade: boolean;

  constructor(init?: Partial<Alert>) {
    this.id = "";
    this.type = AlertType.Error;
    this.message = "";
    this.autoClose = true;
    this.keepAfterRouteChange = false;
    this.fade = false;

    Object.assign(this, init);
  }
}

export enum AlertType {
  Success,
  Error,
  Info,
  Warning
}
