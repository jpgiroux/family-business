import { Injectable } from "@angular/core";
import { Debt } from "../debt";
import { FamilyMember } from "../familyMember";
import { BaseApiSerializer } from "./baseApiSerializer";

@Injectable({
    providedIn: 'root'
})
export class DebtSerializer extends BaseApiSerializer<Debt> {
    constructor() {
        super();
    }

    override fromJson(object: any): Debt
    {
        return {
          owed : object.owed,
          owedBy : object.owed_by == null ? new FamilyMember() : new FamilyMember(object.owed_by.id, object.owed_by.name, object.owed_by.is_parent),
          owedTo : object.owed_to.id == null ? new FamilyMember() : new FamilyMember(object.owed_to.id, object.owed_to.name, object.owed_to.is_parent),
        } as Debt;
    }

    override toJson(object: Debt): any
    {
        return {

        };
    }
}
