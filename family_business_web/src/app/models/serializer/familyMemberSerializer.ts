import { Injectable } from "@angular/core";
import { FamilyMember } from "../familyMember";
import { BaseApiSerializer } from "./baseApiSerializer";

@Injectable({
    providedIn: 'root'
})
export class FamilyMemberSerializer extends BaseApiSerializer<FamilyMember> {
    constructor() {
        super();
    }

    override fromJson(object: any): FamilyMember
    {
        return {
            id: object.id,
            name: object.name,
            is_parent: object.is_parent
        } as FamilyMember;
    }

    override toJson(object: FamilyMember): any
    {
        return {
          id: object.id,
          name: object.name,
          is_parent: object.is_parent
        };
    }
}
