import { Injectable } from "@angular/core";
import { Usager } from "../usager";
import { BaseApiSerializer } from "./baseApiSerializer";

@Injectable({
  providedIn: 'root'
})
export class UsagerApiSerializer extends BaseApiSerializer<Usager> {
  constructor() {
    super();
  }
  // Convertit l'objet provenant du serveur en objet de type T
  public fromJson(object: any): Usager {

    return {
      id: object.id,
      nom_usager: object.nom_usager, // Le fameux mapping
      mot_passe: object.mot_passe,
      nom: object.nom,
      prenom: object.prenom,
      id_compagnie: object.id_compagnie,
      id_role: object.id_role
    } as Usager;
  }

  // Convertit l'objet de type T en objet json correspondant à ce que le serveur attend
  public toJson(object: Usager): any {
    return {
      id: object.id,
      nom_usager: object.nom_usager, // Le fameux mapping
      mot_passe: object.mot_passe,
      nom: object.nom,
      prenom: object.prenom,
      id_compagnie: object.id_compagnie,
      id_role: object.id_role
    };
  }
}
