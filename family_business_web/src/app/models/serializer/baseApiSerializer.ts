import { BaseModel } from "../baseModel";

export abstract class BaseApiSerializer<Model extends BaseModel> {

  public fromJson(object: any): Model {
    return object as Model;
  }

  public toJson(object: Model): any {
    return object;
  }
}
