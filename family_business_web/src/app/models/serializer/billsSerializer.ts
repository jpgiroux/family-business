import { DateConverter } from './../../class/dateConverter';
import { Injectable } from "@angular/core";
import { Bills } from "../bills";
import { BaseApiSerializer } from "./baseApiSerializer";
import { Ratios } from '../ratios';

@Injectable({
    providedIn: 'root'
})
export class BillsSerializer extends BaseApiSerializer<Bills> {
    constructor(private dateConverter: DateConverter) {
        super();
    }

    override fromJson(object: {id: number, payer_id: number, amount: number,
      payee: string,
      ratios: {parent_id: number, value:number}[],
      description: string,
      date: any,
      resolve_date: any}): Bills
    {
        let bill: Bills =  {
            id: object.id,
            payer_id: object.payer_id,
            amount: object.amount,
            payee: object.payee,
            description: object.description,
            date: this.dateConverter.convertDateToString(object.date),
            resolve_date: this.dateConverter.convertDateToString(object.resolve_date),
        } as Bills;

        bill.ratios = new Array<Ratios>();
        object.ratios.forEach(ratios => {
          bill.ratios.push(ratios);
        });

        return bill;
    }

    override toJson(object: Bills): any
    {
      let bill: {id: number, payer_id: number, amount: number,
        payee: string,
        ratios: {parent_id: number, value:number}[],
        description: string,
        date: any,
        resolve_date: any} =
        {
          id: object.id,
          payer_id: object.payer_id,
          amount: object.amount,
          payee: object.payee,
          description: object.description,
          date: this.dateConverter.convertStringToDate(object.date),
          resolve_date: object.resolve_date == "" ? null :this.dateConverter.convertStringToDate(object.resolve_date),
          ratios: new Array<{parent_id: number, value:number}>()
      };

      object.ratios.forEach(ratios => {
        bill.ratios.push(ratios);
      });

      return bill;
    }
}
