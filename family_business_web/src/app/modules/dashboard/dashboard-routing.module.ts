import { DashboardComponent } from './../../components/dashboard/dashboard/dashboard.component';
import { BaseTemplateComponent } from '../../components/base-template/base-template.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component:BaseTemplateComponent, children :[
    { path: '', pathMatch: 'full', component: DashboardComponent}
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
