import { DashboardRoutingModule } from './dashboard-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertModule } from '../alert.module';



@NgModule({
  declarations: [],
  imports: [
    AlertModule,
    CommonModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
