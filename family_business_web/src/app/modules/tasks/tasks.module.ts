import { TasksRoutingModule } from './tasks-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClarityModule } from '@clr/angular';
import { AlertModule } from '../alert.module';



@NgModule({
  declarations: [],
  imports: [
    AlertModule,
    CommonModule,
    TasksRoutingModule
  ]
})
export class TasksModule { }
