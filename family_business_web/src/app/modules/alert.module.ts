import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule } from '@clr/angular';
import { AlertComponent } from '../components/alert/alert.component';



@NgModule({
  imports: [CommonModule, ClarityModule],
  declarations: [AlertComponent],
  exports: [AlertComponent]
})
export class AlertModule { }
