import { BillResolvedListComponent } from './../../components/bills/bill-resolved-list/bill-resolved-list.component';
import { BillFormComponent } from './../../components/bills/bill-form/bill-form.component';
import { BillListComponent } from './../../components/bills/bill-list/bill-list.component';
import { BaseTemplateComponent } from './../../components/base-template/base-template.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component:BaseTemplateComponent, children :[
    { path: '', pathMatch: 'full', component: BillListComponent},
    { path: 'add', pathMatch: 'full', component: BillFormComponent},
    { path: 'edit/:id', pathMatch: 'full', component: BillFormComponent},
    { path: 'resolved', pathMatch: 'full', component: BillResolvedListComponent}
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillsRoutingModule { }
