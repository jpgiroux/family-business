import { DebtService } from './../../services/debt.service';
import { FamilyMemberService } from './../../services/familyMember.service';
import { IFamilyMemberService } from 'src/app/services/interfaces/IFamilyMember.service';
import { BillsService } from "src/app/services/bills.service"
import { IBillsService } from "src/app/services/interfaces/IBills.service"
import { MockBillsService } from "src/app/services/mock/MockBills.service"
import { MockFamilyMemberService } from 'src/app/services/mock/MockFamilyMember.service';
import { IDebtService } from 'src/app/services/interfaces/IDebt.service';
import { MockDebtService } from 'src/app/services/mock/MockDebt.service';


const production = [
  { provide: IBillsService, useClass: BillsService },
  { provide: IFamilyMemberService, useClass: FamilyMemberService },
  { provide: IDebtService, useClass: DebtService },
]

const test = [
  { provide: IBillsService, useClass: MockBillsService },
  { provide: IFamilyMemberService, useClass: MockFamilyMemberService },
  { provide: IDebtService, useClass: MockDebtService },
]


export { test, production }
