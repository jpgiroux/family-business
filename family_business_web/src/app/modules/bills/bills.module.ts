import { BillResolvedListComponent } from './../../components/bills/bill-resolved-list/bill-resolved-list.component';
import { BillFormComponent } from './../../components/bills/bill-form/bill-form.component';
import { BillListComponent } from './../../components/bills/bill-list/bill-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillsRoutingModule} from './bills-routing.module'
import { AlertModule } from '../alert.module';
import { FormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { environment } from 'src/environments/environment';
import { test, production} from './bills-injection'
import { DebtSummaryComponent } from 'src/app/components/debts/debt-summary/debt-summary.component';


@NgModule({
  declarations: [
    BillListComponent,
    BillFormComponent,
    BillResolvedListComponent,
    DebtSummaryComponent,
  ],
  imports: [
    AlertModule,
    CommonModule,
    ClarityModule,
    FormsModule,

    BillsRoutingModule,

  ],
  providers: [
    environment.useMock ? test : production,
  ]
})
export class BillsModule { }
