export class ApiParam
{
  constructor(
    public name: string = "",
    public value: string = "")
    {}

  toUrlParam(): string{
    return "name=" + this.name + "&value=" + this.value;
  }
}
