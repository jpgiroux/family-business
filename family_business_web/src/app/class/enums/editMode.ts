export class EditMode
{
  public static ADD: string = "add";
  public static EDIT: string = "edit";
}
