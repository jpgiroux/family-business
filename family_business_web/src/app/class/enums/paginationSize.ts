export enum PaginationSize{
  TWENTY_FIVE = "TwentyFive",
  FIFTY = "Fifty"
}
