import { SortOrder } from "./enums/sortOrder";

export class SortParam
{
  constructor(
    public field: string = "",
    public ordering: SortOrder = SortOrder.ASC)
    {}

    toUrlParam(): string{
      return "sort.field=" + this.field + "&sort.ordering=" + this.ordering;
    }
}
