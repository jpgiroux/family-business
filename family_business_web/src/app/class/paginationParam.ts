import { PaginationSize } from './enums/paginationSize';
export class PaginationParam
{
  constructor(
    public page: number = 0,
    public pageSize: PaginationSize = PaginationSize.TWENTY_FIVE)
    {}

    toUrlParam(): string{
      return "pagination.page=" + this.page + "&pagination.size=" + this.pageSize;
    }

    public static GetPaginationSize(size: number): PaginationSize | null{
      switch(size){
        case 25:
          return PaginationSize.TWENTY_FIVE;
        case 50:
          return PaginationSize.FIFTY;
        default:
          return null;
      }
    }
}
