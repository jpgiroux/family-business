import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateConverter {
  constructor(private datePipe: DatePipe) { }

  convertDateToString(date: any): string {
    if (date == null || date == "") return "";

    return this.datePipe.transform(date, 'MM/dd/YYYY') || "";
  }

  convertStringToDate(date: any): any {
    if (date == null || date == "") return "";

    let d: Date = new Date(date);
    return this.datePipe.transform(d, 'YYYY-MM-dd') || "";
  }

  getTodayStr(format?: string): string{

    format ? format : 'MM/dd/YYYY';
    return this.datePipe.transform(new Date(), format) || "";
  }
}
