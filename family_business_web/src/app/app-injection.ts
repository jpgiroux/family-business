import { AuthentificationService } from "./services/authentification.service"
import { IAuthentificationService } from './services/interfaces/IAuthentification.service';
import { MockAuthentificationService } from './services/mock/MockAuthentification.service';

const production = [
  { provide: IAuthentificationService, useClass: AuthentificationService },
]

const test = [
  { provide: IAuthentificationService, useClass: MockAuthentificationService },
]


export { test, production }
