import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DebtSummaryComponent } from './debt-summary.component';
import { test } from 'src/app/modules/bills/bills-injection';

describe('DebtSummaryComponent', () => {
  let component: DebtSummaryComponent;
  let fixture: ComponentFixture<DebtSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DebtSummaryComponent ],
      imports: [],
      providers: [
        test]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DebtSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
