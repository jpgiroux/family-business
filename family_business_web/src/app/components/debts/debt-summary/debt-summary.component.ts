import { Component, OnInit } from '@angular/core';
import { Debt } from 'src/app/models/debt';
import { IDebtService } from 'src/app/services/interfaces/IDebt.service';

@Component({
  selector: 'app-debt-summary',
  templateUrl: './debt-summary.component.html',
  styleUrls: ['./debt-summary.component.css']
})
export class DebtSummaryComponent implements OnInit {

  public listComputedDebts: Debt[] = new Array<Debt>();
  public listDebts: Debt[] = new Array<Debt>();

  public isExplain: boolean = false;

  constructor(
    private debtService: IDebtService) { }

  ngOnInit(): void {
    this.debtService.getAll().subscribe(data => {
      this.listDebts = data;
    });

    this.debtService.getComputedDebts().subscribe(data => {
      this.listComputedDebts = data;
    });
  }

}
