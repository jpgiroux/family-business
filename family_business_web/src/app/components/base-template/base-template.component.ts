import { IAuthentificationService } from 'src/app/services/interfaces/IAuthentification.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-base-template',
  templateUrl: './base-template.component.html',
  styleUrls: ['./base-template.component.css']
})
export class BaseTemplateComponent implements OnInit {
  sideMenuCollapsed: boolean = true;

  constructor(
    private authService: IAuthentificationService,
    private router: Router) { }

  ngOnInit(): void {
  }

  onDisconnect(){
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}


