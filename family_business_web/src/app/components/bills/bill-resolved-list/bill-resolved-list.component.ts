import { Component, OnInit } from '@angular/core';
import { ClrDatagridStateInterface } from '@clr/angular/data/datagrid';
import { PaginationSize } from 'src/app/class/enums/paginationSize';
import { SortOrder } from 'src/app/class/enums/sortOrder';
import { PaginationParam } from 'src/app/class/paginationParam';
import { SortParam } from 'src/app/class/sortParam';
import { Bills, BillsSortField } from 'src/app/models/bills';
import { FamilyMember } from 'src/app/models/familyMember';
import { AlertService } from 'src/app/services/alert.service';
import { IBillsService } from 'src/app/services/interfaces/IBills.service';
import { IFamilyMemberService } from 'src/app/services/interfaces/IFamilyMember.service';

@Component({
  selector: 'app-bill-resolved-list',
  templateUrl: './bill-resolved-list.component.html',
  styleUrls: ['./bill-resolved-list.component.css']
})
export class BillResolvedListComponent implements OnInit {

  public listBills: Bills[] = new Array<Bills>();
  public listFamilyMember: FamilyMember[] = new Array<FamilyMember>();
  public total: number = 0;
  public loading: boolean = true;

  constructor(
    private billsService: IBillsService,
    private familyMemberService: IFamilyMemberService,

    private alertService: AlertService) { }

  ngOnInit(): void {

    this.loadInformation();
  }

  getFamilyMember(id: number): FamilyMember{
    let results: FamilyMember[] = this.listFamilyMember.filter(item => item.id == id);
    return results.length > 0 ? results[0] : new FamilyMember();
  }

  refreshBills(state: ClrDatagridStateInterface){
    this.loading = true;

    if(state.sort != undefined && !(<any>Object).values(BillsSortField).includes(state.sort?.by.toString() ?? "")){
      this.alertService.error($localize `Sort field not available for this object`);
      this.loading = false;
      return;
    }

    this.billsService.resolved(
      new SortParam(state.sort?.by.toString(), state.sort?.reverse ? SortOrder.DESC : SortOrder.ASC),
      new PaginationParam((state.page?.current ?? 1) -1, PaginationParam.GetPaginationSize(state.page?.size ?? 0) ?? PaginationSize.TWENTY_FIVE)
    ).subscribe(data => {
      this.listBills = data.results;
      this.total = data.count;
      this.loading = false;
    });
  }

  loadInformation(){
    this.familyMemberService.getAll().subscribe(data => {
      this.listFamilyMember = data;
    });
  }
}
