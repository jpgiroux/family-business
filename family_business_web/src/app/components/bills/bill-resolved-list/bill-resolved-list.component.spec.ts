import { DatePipe } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ClarityModule } from '@clr/angular';
import { AlertModule } from 'src/app/modules/alert.module';
import { test } from 'src/app/modules/bills/bills-injection';

import { BillResolvedListComponent } from './bill-resolved-list.component';

describe('BillResolvedListComponent', () => {
  let component: BillResolvedListComponent;
  let fixture: ComponentFixture<BillResolvedListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillResolvedListComponent ],
      imports: [
        AlertModule,
        RouterTestingModule,
        ClarityModule],
      providers: [
        test,
        DatePipe]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillResolvedListComponent);
    component = fixture.componentInstance;
    fixture.componentInstance.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
