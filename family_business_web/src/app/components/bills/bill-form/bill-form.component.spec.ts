import { ClarityModule } from '@clr/angular';
import { FormsModule } from '@angular/forms';
import { AlertModule } from './../../../modules/alert.module';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { test } from 'src/app/modules/bills/bills-injection';

import { BillFormComponent } from './bill-form.component';
import { BillsRoutingModule } from 'src/app/modules/bills/bills-routing.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('BillFormComponent', () => {
  let component: BillFormComponent;
  let fixture: ComponentFixture<BillFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillFormComponent ],
      imports: [
        AlertModule,
        FormsModule,
        RouterTestingModule,
        ClarityModule],
      providers: [test]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
