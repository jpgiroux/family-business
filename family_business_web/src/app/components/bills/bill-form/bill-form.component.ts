import { IFamilyMemberService } from 'src/app/services/interfaces/IFamilyMember.service';
import { FamilyMember } from './../../../models/familyMember';
import { AlertService } from './../../../services/alert.service';
import { EditMode } from '../../../class/enums/editMode';
import { Component, OnInit } from '@angular/core';
import { Bills } from 'src/app/models/bills';
import { ActivatedRoute, Router } from '@angular/router';
import { IBillsService } from 'src/app/services/interfaces/IBills.service';
import { NgForm } from '@angular/forms';

import '@cds/core/range/register.js';
import { Ratios } from 'src/app/models/ratios';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-bill-form',
  templateUrl: './bill-form.component.html',
  styleUrls: ['./bill-form.component.css']
})
export class BillFormComponent implements OnInit {


  public editMode : EditMode = EditMode.ADD;
  public bills: Bills = new Bills();

  public ratio: number = 0.5;
  public parents: FamilyMember[] = new Array<FamilyMember>();

  constructor(
    private billsService: IBillsService,
    private familyMemberService: IFamilyMemberService,
    private alertService: AlertService,

    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {

    this.familyMemberService.getAll().subscribe(data => {
      this.parents = new Array<FamilyMember>();
      data.forEach(member => {
        if(member.is_parent){
          this.parents.push(member);
        }
      });

      this.activatedRoute.paramMap.subscribe(params => {
        let id = params.get('id');
        if (id != null) {
          this.loadBill(+id);
        }
      });
    })
  }

  loadBill(id: number){
      this.billsService.get(id).subscribe(data => {
        this.editMode = EditMode.EDIT;
        this.bills = data;

        if(this.bills.ratios.length > 0){
          this.ratio = 1 - (this.bills.ratios.find(r => r.parent_id = this.parents[0].id)?.value ?? 0);
        }

        console.log(this.bills);
      });
  }

  onSubmit(form: NgForm) {

    if (form.invalid) {
      return false;
    }

    this.bills.ratios = new Array<Ratios>();
    this.bills.ratios.push(new Ratios(this.parents[0].id, (1 - this.ratio)));
    this.bills.ratios.push(new Ratios(this.parents[1].id, this.ratio));

    let billsSub: Observable<Bills> = new Observable<Bills>();
    if (this.bills.id == 0) {
      billsSub = this.billsService.add(this.bills)
    } else {
      billsSub = this.billsService.update(this.bills.id, this.bills)
    }

    billsSub.subscribe(data => {
        this.router.navigate(['bills']);
        this.alertService.success($localize `Bills saved`);
    });

    return true;
  }

  supprimerBill(){
    this.billsService.delete(this.bills.id).subscribe(data => {
      this.alertService.success($localize `Bills deleted`, { keepAfterRouteChange: true});
      this.router.navigate(['bills']);
    });
  }

  onCancel(){
    this.router.navigate(['bills']);
  }

  isEditMode(): boolean{
    return this.editMode == EditMode.EDIT;
  }
}
