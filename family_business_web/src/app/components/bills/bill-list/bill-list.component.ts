import { AlertService } from './../../../services/alert.service';
import { DateConverter } from './../../../class/dateConverter';
import { Router } from '@angular/router';
import { IDebtService } from 'src/app/services/interfaces/IDebt.service';
import { FamilyMember } from './../../../models/familyMember';
import { IBillsService } from '../../../services/interfaces/IBills.service';
import { Component, OnInit } from '@angular/core';
import { Bills } from 'src/app/models/bills';
import { IFamilyMemberService } from 'src/app/services/interfaces/IFamilyMember.service';
import { Debt } from 'src/app/models/debt';


@Component({
  selector: 'app-bill-list',
  templateUrl: './bill-list.component.html',
  styleUrls: ['./bill-list.component.css']
})
export class BillListComponent implements OnInit {

  public listBills: Bills[] = new Array<Bills>();
  public listBillsResolved: Bills[] = new Array<Bills>();
  public listFamilyMember: FamilyMember[] = new Array<FamilyMember>();

  public selectedBills: Bills = new Bills();

  constructor(
    private billsService: IBillsService,
    private familyMemberService: IFamilyMemberService,

    private dateConverter: DateConverter,
    private alertService: AlertService,

    private router: Router) { }

  ngOnInit(): void {

    this.loadInformation();
  }

  getFamilyMember(id: number): FamilyMember{
    let results: FamilyMember[] = this.listFamilyMember.filter(item => item.id == id);
    return results.length > 0 ? results[0] : new FamilyMember();
  }

  addBills(){
    this.router.navigate(["bills","add"]);
  }

  editBills(){
    if(this.selectedBills.id == 0){
      return;
    }

    this.router.navigate(["bills","edit", this.selectedBills.id]);
  }

  deleteBills(){
    if(this.selectedBills.id == 0){
      return;
    }

    this.billsService.delete(this.selectedBills.id).subscribe(data =>{
      this.loadInformation();
    });
  }

  resolveBills(){
    let count = 0;
    this.listBills.forEach(bills => {
      bills.resolve_date = this.dateConverter.getTodayStr();

      this.billsService.update(bills.id, bills).subscribe({
        next: (data) => {
          if(data.id != 0){
            count++;
            if(count == this.listBills.length){
              this.alertService.success($localize `Bills resolved`);

              this.loadInformation();
            }
          }
        },
        error: (e) => {
          this.alertService.error($localize `Errors during bills resolved`);
          console.log(e);
        },
        complete: () => console.info('complete')
      });
    });
  }

  selectBills(bills: Bills){
    this.selectedBills = bills;
    console.log(bills);
  }

  loadInformation(){
    this.billsService.getAll().subscribe(data => {
      this.listBills = data.filter(bill => bill.resolve_date == "");
    });

    this.familyMemberService.getAll().subscribe(data => {
      this.listFamilyMember = data;
    });
  }
}
