import { ClarityModule } from '@clr/angular';
import { CommonModule, DatePipe } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AlertModule } from 'src/app/modules/alert.module';
import { test } from 'src/app/modules/bills/bills-injection';

import { BillListComponent } from './bill-list.component';

describe('BillListComponent', () => {
  let component: BillListComponent;
  let fixture: ComponentFixture<BillListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillListComponent ],
      imports: [
        AlertModule,
        RouterTestingModule,
        ClarityModule],
      providers: [
        test,
        DatePipe]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillListComponent);
    component = fixture.componentInstance;
    fixture.componentInstance.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
