import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { UserLogin } from 'src/app/models/userLogin';
import { AlertService } from 'src/app/services/alert.service';
import { IAuthentificationService } from 'src/app/services/interfaces/IAuthentification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  userLogin = new UserLogin();
  submitted = false;

  constructor(
    private authService: IAuthentificationService,

    private logger: NGXLogger,
    private router: Router,

    private alertService: AlertService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.submitted = true;
    this.login(new UserLogin(0, this.userLogin.email, this.userLogin.password));
  }

  login(userLogin: UserLogin) {

    this.authService.login(userLogin).subscribe({
      next: (isAuthenticated: boolean) => {
        console.log(isAuthenticated);
        if (isAuthenticated) {
          this.router.navigate(['/dashboard']);
        } else {
          this.alertService.error("Mauvais nom d'usager ou mot de passe", { autoClose: true });
        }
      },
      error: (e: any) => {
        console.error(e);
        this.alertService.error("Erreur de connection au serveur", { autoClose: true });
      },
      complete: () => console.info('login complete')
    });
  }

}
