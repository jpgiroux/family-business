import { BaseTemplateComponent } from './components/base-template/base-template.component';
import { BrowserModule } from '@angular/platform-browser';
import { DEFAULT_CURRENCY_CODE, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { AuthGuard } from './guards/auth.guard';
import { AuthentificationService } from './services/authentification.service';
import { CustomHttpInterceptor } from './middleware/customHttpInterceptor';
import { AlertModule } from './modules/alert.module';
import { DashboardComponent } from './components/dashboard/dashboard/dashboard.component';
import { environment } from 'src/environments/environment';
import { test, production } from './app-injection';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    BaseTemplateComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ClarityModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,

    AlertModule,
    LoggerModule.forRoot({ serverLoggingUrl: '/api/logs', level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.ERROR }),
  ],
  providers: [
    AuthGuard,
    AuthentificationService,
    environment.useMock ? test : production,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CustomHttpInterceptor,
      multi: true
    },
    {provide: DEFAULT_CURRENCY_CODE, useValue: 'CAD'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
