import { FamilyMember } from './../../models/familyMember';
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { ApiParam } from "src/app/class/apiParam";
import { Debt } from "../../models/debt";
import { IDebtService } from "../interfaces/IDebt.service";
import { MockService } from "./mock.service";

@Injectable({
    providedIn: 'root'
})
export class MockDebtService extends MockService<Debt> implements IDebtService {

  constructor() {
      super(Debt);
      this.BD.push(new Debt(new FamilyMember(4, "Ludo", false), new FamilyMember(1, "JP", true), 25.505001));
      this.BD.push(new Debt(new FamilyMember(3, "Olie", false), new FamilyMember(2, "Clau", true), 86.205));
      this.BD.push(new Debt(new FamilyMember(4, "Ludo", false), new FamilyMember(2, "Clau", true), 86.205));
      this.BD.push(new Debt(new FamilyMember(3, "Olie", false), new FamilyMember(1, "JP", true), 25.505001));
      this.BD.push(new Debt(new FamilyMember(2, "Clau", true), new FamilyMember(1, "JP", true), 1140.47));
      this.BD.push(new Debt(new FamilyMember(1, "JP", true), new FamilyMember(2, "Clau", true), 1281.134));
  }

  public getComputedDebts(): Observable<Debt[]> {
    var listComputedDebts: Array<Debt> = new Array<Debt>();
    this.getAll().subscribe(data => {
      data.forEach(debt => {
        var newDebt: Debt = new Debt(debt.owedBy, debt.owedTo, debt.owed);
        var d = data.find(d => d.owedBy.id == debt.owedTo.id && d.owedTo.id == debt.owedBy.id);

        if(!d){
          listComputedDebts.push(newDebt);
          return;
        }

        if(newDebt.owed <= d.owed){
          newDebt.owed = 0;
        }else{
          newDebt.owed = debt.owed - d.owed
        }

        if(newDebt.owed > 0){
          listComputedDebts.push(newDebt);
        }
      });
    });

    return of(listComputedDebts);
  }

  public getWithParams(queryParams: any): Observable<Debt> {
      throw new Error("Method not implemented.");
  }

  public getManyWithParams(queryParams: any): Observable<Debt[]> {
      return this.getListEntity(queryParams);
  }

  public get(id: number): Observable<Debt> {
      return this.getEntityById(id);
  }

  public getAll(): Observable<Debt[]> {
      return this.getAllEntity();
  }

  public add(item: Debt): Observable<Debt> {
      return this.saveEntity(item)
  }

  public update(id: string | number, item:  Debt): Observable<Debt> {
      return this.saveEntity(item);
  }

  public delete(id: number): Observable<Boolean> {
      return this.deleteEntity(id);
  }
}
