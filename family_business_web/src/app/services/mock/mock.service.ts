import { BaseModel } from 'src/app/models/baseModel';
import { Observable, of } from "rxjs";
import { ApiParam } from "src/app/class/apiParam";

export class MockService<T> {

  protected readonly BD: any[] = new Array<any>();

  constructor(private testType: new () => T) { }

  getListEntity(params: ApiParam): Observable<T[]> {
    let results: T[] = this.BD.filter(entity => entity[params.name] == params.value);

    if (results.length == 0) {
      return of(new Array<T>());
    }

    return of([...results]);
  }

  getAllEntity(): Observable<T[]> {
    return of([...this.BD]);
  }

  getEntityById(id: number): Observable<T> {
    let index = this.BD.findIndex(entity => entity.id == id);

    if (index == -1) {
      return of(new this.testType());
    }

    return of(this.BD[index]);
  }

  saveEntity(entity: any): Observable<T> {

    if (entity.id == 0) {

      let newId = this.BD.length == 0 ? 1 : (Math.max.apply(Math, this.BD.map(function (dossier) { return dossier.id; })) + 1);
      entity.id = newId;
      this.BD.push(entity);

    } else {

      let index = this.BD.findIndex(dossier => dossier.id == entity.id);

      if (index == -1) {

        let newId = this.BD.length == 0 ? 1 : (Math.max.apply(Math, this.BD.map(function (dossier) { return dossier.id; })) + 1);
        entity.id = newId;
        this.BD.push(entity);

      } else {
        this.BD[index] = entity;
      }
    }

    return of(entity);
  }

  deleteEntity(id: number): Observable<Boolean> {
    let index = this.BD.findIndex(row => row.id == id);

    if (index > -1) {
      this.BD.splice(index, 1);
      return of(true);
    }
    return of(false);
  }
}
