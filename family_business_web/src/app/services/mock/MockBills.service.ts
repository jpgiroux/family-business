import { GridListResults } from 'src/app/class/gridListResults';
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { ApiParam } from "src/app/class/apiParam";
import { PaginationParam } from "src/app/class/paginationParam";
import { SortParam } from "src/app/class/sortParam";
import { Ratios } from "src/app/models/ratios";
import { Bills } from "../../models/bills";
import { IBillsService } from "../interfaces/IBills.service";
import { MockService } from "./mock.service";

@Injectable({
    providedIn: 'root'
})
export class MockBillsService extends MockService<Bills> implements IBillsService {

  constructor() {
      super(Bills);
      this.BD.push(new Bills(1,1,182.86,"All", [new Ratios(1,0.6), new Ratios(2,0.4)], "Metro", "01/15/2022", ""));
      this.BD.push(new Bills(2,2,54.00,"All", [new Ratios(1,0.5), new Ratios(2,0.5)], "Essence", "01/14/2022", ""));
      this.BD.push(new Bills(3,1,136.41,"All", [new Ratios(1,0.6), new Ratios(2,0.4)], "IGA", "01/08/2022", ""));
      this.BD.push(new Bills(4,1,27.45,"All", [new Ratios(1,0.0), new Ratios(2,1)], "Jean-Cout", "01/07/2022", ""));
      this.BD.push(new Bills(5,2,48.01,"All", [new Ratios(1,0.5), new Ratios(2,0.5)], "Gaz", "01/07/2022", ""));
  }

  public resolved(sort: SortParam, pagination: PaginationParam): Observable<GridListResults> {
    var results: GridListResults = {
      count: this.BD.length,
      results: this.BD
    }

    return of(results);
  }

  public getWithParams(queryParams: any): Observable<Bills> {
    throw new Error("Method not implemented.");
  }

  public getManyWithParams(queryParams: any): Observable<Bills[]> {
    return this.getListEntity(queryParams);
  }

  public get(id: number): Observable<Bills> {
    return this.getEntityById(id);
  }

  public getAll(): Observable<Bills[]> {
    return this.getAllEntity();
  }

  public add(item: Bills): Observable<Bills> {
    return this.saveEntity(item)
  }

  public update(id: string | number, item:  Bills): Observable<Bills> {
    return this.saveEntity(item);
  }

  public delete(id: number): Observable<Boolean> {
    return this.deleteEntity(id);
  }
}
