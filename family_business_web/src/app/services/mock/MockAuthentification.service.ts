import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { IAuthentificationService } from '../interfaces/IAuthentification.service';
import { UserLogin } from 'src/app/models/userLogin';

@Injectable()
export class MockAuthentificationService implements IAuthentificationService {
  private readonly token: string = "J'aime les jp";

  constructor() { }

  public getToken(): string {
    return localStorage.getItem('token') ?? "";
  }

  public getLoggedUsagerId(): number {
    return 1;
  }

  public isAuthenticated(): boolean {
    return this.getToken() == this.token;
  }

  public login(userLogin: UserLogin): Observable<boolean> {
    if (userLogin.email == "admin") {
      userLogin.id = 1;
      userLogin.token = this.token;

      localStorage.setItem('token', this.token);

      return of(true);
    }

    return of(false);
  }

  logout(): void {
    localStorage.clear();
  }
}
