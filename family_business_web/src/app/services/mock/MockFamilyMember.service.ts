import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { ApiParam } from "src/app/class/apiParam";
import { FamilyMember } from "../../models/familyMember";
import { IFamilyMemberService } from "../interfaces/IFamilyMember.service";
import { MockService } from "./mock.service";

@Injectable({
    providedIn: 'root'
})
export class MockFamilyMemberService extends MockService<FamilyMember> implements IFamilyMemberService {

    constructor() {
        super(FamilyMember);
        this.BD.push(new FamilyMember(1, "JP", true));
        this.BD.push(new FamilyMember(2, "Clau", true));
        this.BD.push(new FamilyMember(3, "Olie", false));
        this.BD.push(new FamilyMember(4, "Ludo", false));
    }

    public getWithParams(queryParams: any): Observable<FamilyMember> {
        throw new Error("Method not implemented.");
    }

    public getManyWithParams(queryParams: any): Observable<FamilyMember[]> {
        return this.getListEntity(queryParams);
    }

    public get(id: number): Observable<FamilyMember> {
        return this.getEntityById(id);
    }

    public getAll(): Observable<FamilyMember[]> {
        return this.getAllEntity();
    }

    public add(item: FamilyMember): Observable<FamilyMember> {
        return this.saveEntity(item)
    }

    public update(id: string | number, item:  FamilyMember): Observable<FamilyMember> {
        return this.saveEntity(item);
    }

    public delete(id: number): Observable<Boolean> {
        return this.deleteEntity(id);
    }
}
