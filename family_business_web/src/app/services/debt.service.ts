import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { DebtSerializer } from "../models/serializer/debtSerializer";
import { Debt } from "../models/debt";
import { IDebtService } from "./interfaces/IDebt.service";
import { ApiService } from "./apiService.service";
import { Observable, of } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class DebtService extends ApiService<Debt> implements IDebtService {

  constructor(httpClient: HttpClient, apiSerializer: DebtSerializer) {
      super(httpClient, apiSerializer, 'debts');
  }

  public getComputedDebts(): Observable<Debt[]> {
    var listComputedDebts: Array<Debt> = new Array<Debt>();
    this.getAll().subscribe(data => {
      data.forEach(debt => {
        var newDebt: Debt = new Debt(debt.owedBy, debt.owedTo, debt.owed);
        var d = data.find(d => d.owedBy.id == debt.owedTo.id && d.owedTo.id == debt.owedBy.id);

        if(!d){
          listComputedDebts.push(newDebt);
          return;
        }

        if(newDebt.owed <= d.owed){
          newDebt.owed = 0;
        }else{
          newDebt.owed = debt.owed - d.owed
        }

        if(newDebt.owed > 0){
          listComputedDebts.push(newDebt);
        }
      });
    });

    return of(listComputedDebts);
  }
}
