import { GridListResults } from '../class/gridListResults';
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { BillsSerializer } from "../models/serializer/billsSerializer";
import { Bills } from "../models/bills";
import { IBillsService } from "./interfaces/IBills.service";
import { ApiService } from "./apiService.service";
import { map, Observable, tap } from "rxjs";
import { PaginationParam } from "../class/paginationParam";
import { SortParam } from "../class/sortParam";

@Injectable({
    providedIn: 'root'
})
export class BillsService extends ApiService<Bills> implements IBillsService {

  constructor(httpClient: HttpClient, apiSerializer: BillsSerializer) {
    super(httpClient, apiSerializer, 'bills');
  }

  public resolved(sort: SortParam, pagination: PaginationParam): Observable<GridListResults> {
    const url = `${this._baseUrl + '/resolved?'}${pagination.toUrlParam()}&${sort.toUrlParam()}`

    return this._httpClient.get<any>(url).pipe(
      tap((results: any) => {
        console.log(results);
      }),
      map((resultsTemp: any) => {
        var results: GridListResults = {
          count: resultsTemp.total_elements,
          results: this.convertDataList(resultsTemp.bills)
        };
        return results;
      }));
  }
}
