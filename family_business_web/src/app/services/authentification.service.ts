import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { UserLogin } from '../models/userLogin';
import { observable, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import decode from 'jwt-decode';

@Injectable()
export class AuthentificationService {
  public _endpoint: string = "users_sessions";

  constructor(private http: HttpClient) { }

  public getToken(): string {
    return localStorage.getItem('token') ?? "";
  }

  public isAuthenticated(): boolean {
    return this.getToken() != "";
  }

  public getLoggedUsagerId(): number {
    let token = this.getToken();

    if (token == "") {
      return 0;
    }

    try {
      let decodedToken: any = decode(token);
      return +(decodedToken?.nameid ?? 0);

    } catch (error) {
      console.log(error);
      return 0;
    }

  }

  protected get _baseUrl(): string {
    return `${environment.apiUrl}/${this._endpoint}`;
  }


  public login(userLogin: UserLogin): Observable<boolean> {

    return this.http.post<any>(this._baseUrl , { email: userLogin.email, password: userLogin.password })
      .pipe(
        map(data => {
          if (data == null || data.token == null || data.token == '') {
            return false;
          }

          localStorage.setItem('token', data.token);

          return true;

        })
      );
  }

  public logout(): void {
    localStorage.clear();
  }
}
