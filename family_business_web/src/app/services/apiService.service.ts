import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { BaseModel } from "../models/baseModel";
import { environment } from "src/environments/environment";
import { map, tap } from "rxjs/operators";
import { ApiParam } from "../class/apiParam";
import { BaseApiSerializer } from "../models/serializer/baseApiSerializer";

export class ApiService<T extends BaseModel> {

  protected get _baseUrl(): string {
    return `${environment.apiUrl}/${this._endpoint}`;
  }

  constructor(
    protected readonly _httpClient: HttpClient,
    protected readonly _apiSerializer: BaseApiSerializer<T>,

    private readonly _endpoint: string,
  ) {
  }
  protected convertData(response: any): T {

    if (response == null) {
      return {} as T;
    }

    return this._apiSerializer.fromJson(response) as T;
  }

  protected convertDataList(response: any): T[] {
    if (response == null) {
      return new Array<T>();
    }

    return response.map((result: any) => {
      return this._apiSerializer.fromJson(result) as T;
    });
  }

  public add(item: T): Observable<T> {
    return this._httpClient.post(this._baseUrl, this._apiSerializer.toJson(item)).pipe(
      tap((data: any) => {
      }),
      map((data: any) => this.convertData(data)));
  }

  // Récupère une ressource par son identifiant
  public get(id: number | string): Observable<T> {
    const url = `${this._baseUrl}/${id}`;
    return this._httpClient.get<T>(url).pipe(
      tap((data: any) => {
      }),
      map((data: any) => this.convertData(data)));
  }

  // Récupère une ressources en fonction des paramètres passés
  public getWithParams(queryParams: ApiParam): Observable<T> {
    const url = `${this._baseUrl + '/byfield?'}${queryParams.toUrlParam()};`

    return this._httpClient.get<T>(url).pipe(
      tap((data: any) => {
      }),
      map((data: any) => this.convertData(data)));
  }

  // Récupère une ou plusieurs ressources en fonction des paramètres passés
  public getManyWithParams(queryParams: ApiParam): Observable<T[]> {
    const url = `${this._baseUrl + '/byfield?'}${queryParams.toUrlParam()};`

    return this._httpClient.get<T[]>(url).pipe(
      tap((data: any) => {
      }),
      map((data: any) => this.convertDataList(data)));
  }

  public getAll(): Observable<T[]> {
    return this._httpClient.get<T[]>(this._baseUrl).pipe(
      tap((data: any) => {
      }),
      map((data: any) => this.convertDataList(data)));
  }

  public update(id: number | string, item: T): Observable<T> {
    return this._httpClient
      .put(`${this._baseUrl}/${id}`, this._apiSerializer.toJson(item))
      .pipe(
        tap((data: any) => {
        }),
        map((data: any) => this.convertData(data))
      );
  }

  public delete(id: number | string): Observable<any> {
    return this._httpClient.delete(`${this._baseUrl}/${id}`);
  }
}
