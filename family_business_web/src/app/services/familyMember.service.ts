import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { FamilyMemberSerializer } from "../models/serializer/familyMemberSerializer";
import { FamilyMember } from "../models/familyMember";
import { IFamilyMemberService } from "./interfaces/IFamilyMember.service";
import { ApiService } from "./apiService.service";

@Injectable({
    providedIn: 'root'
})
export class FamilyMemberService extends ApiService<FamilyMember> implements IFamilyMemberService {

    constructor(httpClient: HttpClient, apiSerializer: FamilyMemberSerializer) {
        super(httpClient, apiSerializer, 'family/members');
    }
}
