import { FamilyMember } from "../../models/familyMember";
import { IBaseService } from "./IBaseService";

export abstract class IFamilyMemberService extends IBaseService<FamilyMember> { }
