import { Observable } from "rxjs";
import { UserLogin } from "src/app/models/userLogin";

export abstract class IAuthentificationService {

  public abstract login(userLogin: UserLogin): Observable<boolean>;
  public abstract logout(): any;
  public abstract isAuthenticated(): boolean;
  public abstract getToken(): string;
}
