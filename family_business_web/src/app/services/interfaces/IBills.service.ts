import { PaginationParam } from './../../class/paginationParam';
import { SortParam } from './../../class/sortParam';
import { Observable } from "rxjs";
import { Bills } from "../../models/bills";
import { IBaseService } from "./IBaseService";
import { GridListResults } from 'src/app/class/gridListResults';

export abstract class IBillsService extends IBaseService<Bills> {

  public abstract resolved(sort: SortParam, pagination: PaginationParam): Observable<GridListResults>;
 }
