import { Observable } from "rxjs";
import { ApiParam } from "src/app/class/apiParam";
import { BaseModel } from "src/app/models/baseModel";

export abstract class IBaseService<T extends BaseModel> {

  public abstract add(item: T): Observable<T>;
  public abstract get(id: number | string, isInCache?: boolean): Observable<T>;
  public abstract getWithParams(queryParams: any, isInCache?: boolean): Observable<T>;
  public abstract getManyWithParams(queryParams: ApiParam, isInCache?: boolean): Observable<T[]>;
  public abstract getAll(isInCache?: boolean): Observable<T[]>;
  public abstract update(id: number | string, item: T): Observable<T>;
  public abstract delete(id: number | string): Observable<Boolean>;
}
