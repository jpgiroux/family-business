import { Observable } from "rxjs";
import { Debt } from "../../models/debt";
import { IBaseService } from "./IBaseService";

export abstract class IDebtService extends IBaseService<Debt> {

  public abstract getComputedDebts(): Observable<Debt[]>;

}
