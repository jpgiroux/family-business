use clap::{Parser, Subcommand};
use config::Config;
use family_business_dal::{Dal, DalError};

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
#[clap(propagate_version = true)]
struct Cli {
    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    GetMembers,
    GetMember { id: i32 },
}

#[tokio::main]
async fn main() -> Result<(), i32> {
    let cli = Cli::parse();

    let settings = Config::builder()
        // Add in `./Settings.toml`
        .add_source(config::File::with_name("family_business_cli/Settings").required(false))
        .add_source(config::Environment::with_prefix("fop_"))
        .build()
        .unwrap();

    let db_url: String = settings
        .get("db_url")
        .expect("db url to be in settings or in ENV");

    let dal = Dal::new(&db_url).await;

    match dal {
        Ok(dal) => {
            let result = handle_command(&cli, &dal).await;
            match result {
                Ok(_) => Ok(()),
                Err(e) => {
                    eprintln!("{:?}", e);
                    Err(1)
                }
            }
        }
        Err(err) => {
            eprintln!("{:?}", err);
            Err(1)
        }
    }
}

async fn handle_command(cli: &Cli, dal: &family_business_dal::Dal) -> Result<(), DalError> {
    match &cli.command {
        Commands::GetMember { id } => {
            let user = dal.get_member(*id).await?;
            println!("User: {:?}", user);
        }
        Commands::GetMembers => {
            let users = dal.get_members().await?;
            users.into_iter().for_each(|u| println!("User: {:?}", u))
        }
    };
    Ok(())
}
