FROM debian:buster-slim as basehost
RUN apt update
RUN apt install -y openssl


FROM rust:1.63 as base

WORKDIR /app
COPY . .
RUN cargo build -r

FROM basehost as release

EXPOSE 8000

COPY --from=base /app/target/release/family_business_api ./family_business_api
COPY --from=base /app/target/release/family_business_cli ./family_business_cli

CMD ["./family_business_api"]
