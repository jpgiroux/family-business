use crate::DalError;
use family_business_lib::bills::{
    Bill, BillResolvedQueryResult, BillSortExpression, BillSortField, BillsResolution, NewBill,
};
use family_business_lib::query::{Ordering, PageSize, Pagination};
use sqlx::postgres::PgRow;
use sqlx::{PgPool, Row};
use std::sync::Arc;

pub struct Bills {
    pool: Arc<PgPool>,
}

impl Bills {
    pub fn new(pool: Arc<PgPool>) -> Self {
        Bills { pool }
    }

    pub async fn create_bill(&self, new_bill: &NewBill) -> Result<Bill, DalError> {
        let id =
        sqlx::query(
            "INSERT INTO bills (payer_id, amount, payee, ratios, description, date) VALUES($1, $2, $3, $4, $5, $6) RETURNING id"
        )
        .bind(&new_bill.payer_id)
        .bind(&new_bill.amount)
        .bind(&new_bill.payee)
        .bind(&serde_json::to_string(&new_bill.ratios).expect("cant parse ratios to json"))
        .bind(&new_bill.description)
        .bind(&new_bill.date)
        .fetch_one(&*self.pool)
        .await?
        .try_get(0)?;

        Ok(Bill {
            id,
            payer_id: new_bill.payer_id,
            amount: new_bill.amount,
            payee: new_bill.payee.clone(),
            ratios: new_bill.ratios.clone(),
            description: new_bill.description.clone(),
            date: new_bill.date,
            resolve_date: None,
        })
    }

    pub async fn get_bill(&self, id: i32) -> Result<Option<Bill>, DalError> {
        let bill = sqlx::query(
            "SELECT id, payer_id, amount, payee, ratios, description, date, resolve_date FROM bills WHERE id = $1"
        )
        .bind(&id)
        .try_map(|row: PgRow| {
            Ok(Bill {
                id : row.try_get(0)?,
                payer_id: row.try_get(1)?,
                amount: row.try_get(2)?,
                payee: row.try_get(3)?,
                ratios: serde_json::from_str(row.try_get(4)?).expect("serde ratio parsed"),
                description: row.try_get(5)?,
                date: row.try_get(6)?,
                resolve_date: row.try_get(7)?,
            })
        })
        .fetch_optional(&*self.pool)
        .await?;

        Ok(bill)
    }

    pub async fn delete_bill(&self, id: i32) -> Result<(), DalError> {
        sqlx::query("DELETE FROM bills WHERE id = $1")
            .bind(&id)
            .execute(&*self.pool)
            .await?;

        Ok(())
    }

    pub async fn edit_bill(&self, bill: &Bill) -> Result<Bill, DalError> {
        sqlx::query(
            "UPDATE bills SET payer_id = $1, amount = $2, payee = $3, ratios = $4, description = $5, date = $6, resolve_date = $7 WHERE id = $8"
        )
        .bind(&bill.payer_id)
        .bind(&bill.amount)
        .bind(&bill.payee)
        .bind(&serde_json::to_string(&bill.ratios).expect("cant parse ratios to json"))
        .bind(&bill.description)
        .bind(&bill.date)
        .bind(&bill.resolve_date)
        .bind(&bill.id)
        .execute(&*self.pool)
        .await?;

        Ok(bill.clone())
    }

    pub async fn get_bills(&self) -> Result<Vec<Bill>, DalError> {
        let results = sqlx::query(
            "SELECT id, payer_id, amount, payee, ratios, description, date, resolve_date FROM bills WHERE resolve_date IS NULL"
        ).try_map(|row: PgRow| {
            Ok(Bill {
                id : row.try_get(0)?,
                payer_id: row.try_get(1)?,
                amount: row.try_get(2)?,
                payee: row.try_get(3)?,
                ratios: serde_json::from_str(row.try_get(4)?).expect("serde ratio parsed"),
                description: row.try_get(5)?,
                date: row.try_get(6)?,
                resolve_date: row.try_get(7)?,
            })
        }).fetch_all(&*self.pool)
        .await?;

        Ok(results)
    }

    pub async fn get_bills_resolved(
        &self,
        pagination: Pagination,
        sort: BillSortExpression,
    ) -> Result<BillResolvedQueryResult, DalError> {
        let page_size = match pagination.size {
            PageSize::TwentyFive => 25,
            PageSize::Fifty => 50,
            PageSize::Invalid => 0,
        };

        let page_exp = format!("LIMIT {} OFFSET {}", page_size, pagination.page * page_size);

        let sort_field = match sort.field {
            BillSortField::PayerId => "payer_id",
            BillSortField::Date => "date",
            BillSortField::ResolveDate => "resolve_date",
            BillSortField::Amount => "amount",
            BillSortField::Description => "description",
        };
        let sort_order = match sort.ordering {
            Ordering::Asc => "asc",
            Ordering::Desc => "desc",
        };

        let sort_exp = format!("ORDER BY {} {}", sort_field, sort_order);

        let count_query = r"
            SELECT COUNT(*) as total
            FROM bills
            WHERE resolve_date IS NOT NULL;"
            .to_string();

        let total_elements = sqlx::query(&count_query)
            .fetch_one(&*self.pool)
            .await?
            .try_get(0)?;

        let query = format!(
            r"
            SELECT id, payer_id, amount, payee, ratios, description, date, resolve_date
            FROM bills
            WHERE resolve_date IS NOT NULL {} {}",
            sort_exp, page_exp
        );

        let bills = sqlx::query(&query)
            .try_map(|row: PgRow| {
                Ok(Bill {
                    id: row.try_get(0)?,
                    payer_id: row.try_get(1)?,
                    amount: row.try_get(2)?,
                    payee: row.try_get(3)?,
                    ratios: serde_json::from_str(row.try_get(4)?).expect("serde ratio parsed"),
                    description: row.try_get(5)?,
                    date: row.try_get(6)?,
                    resolve_date: row.try_get(7)?,
                })
            })
            .fetch_all(&*self.pool)
            .await?;

        Ok(BillResolvedQueryResult {
            bills,
            total_elements,
        })
    }

    pub async fn resolve_bills(&self, bills_resolution: &BillsResolution) -> Result<(), DalError> {
        let in_statement = bills_resolution
            .ids
            .iter()
            .map(|id| id.to_string())
            .collect::<Vec<String>>()
            .join(",");

        sqlx::query(&format!(
            "UPDATE bills SET resolve_date = $1 WHERE id IN ({})",
            in_statement
        ))
        .bind(&bills_resolution.resolve_date)
        .execute(&*self.pool)
        .await?;

        Ok(())
    }
}
