pub mod bills;
pub mod security;
pub mod todo;

use bills::Bills;
use family_business_lib::{FamilyMember, NewFamilyMember};
use security::Security;
use sqlx::{postgres::PgRow, PgPool, Row};
use std::{fmt::Display, sync::Arc};
use todo::Todo;

#[derive(Debug)]
pub enum DalError {
    DbError(String),
}

impl Display for DalError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            DalError::DbError(e) => {
                f.write_str(format!("A DB related error as occured. {:?}", e).as_str())
            }
        }
    }
}

impl From<sqlx::Error> for DalError {
    fn from(se: sqlx::Error) -> Self {
        DalError::DbError(se.to_string())
    }
}

pub struct Dal {
    pool: Arc<PgPool>,
    pub security: Security,
    pub bills: Bills,
    pub todo: Todo,
}

impl Dal {
    pub async fn new(params: &str) -> Result<Self, Box<dyn std::error::Error>> {
        let pool = PgPool::connect(params).await?;
        Ok(Dal {
            pool: Arc::new(pool.clone()),
            security: Security::new(Arc::new(pool.clone())),
            bills: Bills::new(Arc::new(pool.clone())),
            todo: Todo::new(Arc::new(pool)),
        })
    }

    pub async fn get_member(&self, member_id: i32) -> Result<Option<FamilyMember>, DalError> {
        let result =
            sqlx::query("SELECT id, name, user_id, is_parent FROM family_members WHERE id = $1")
                .bind(member_id)
                .try_map(|row: PgRow| {
                    Ok(FamilyMember {
                        id: row.try_get(0)?,
                        name: row.try_get(1)?,
                        user_id: row.try_get(2)?,
                        is_parent: row.try_get(3)?,
                    })
                })
                .fetch_optional(&*self.pool)
                .await?;

        Ok(result)
    }

    pub async fn create_member(&self, new_member: &NewFamilyMember) -> Result<(), DalError> {
        sqlx::query("INSERT INTO family_members(name, is_parent, user_id) VALUES($1, $2, $3)")
            .bind(&new_member.name)
            .bind(&new_member.is_parent)
            .bind(&new_member.user_id)
            .execute(&*self.pool)
            .await?;

        Ok(())
    }

    pub async fn get_members(&self) -> Result<Vec<FamilyMember>, DalError> {
        let results = sqlx::query("SELECT id, name, user_id, is_parent FROM family_members")
            .try_map(|row: PgRow| {
                Ok(FamilyMember {
                    id: row.try_get(0)?,
                    name: row.try_get(1)?,
                    user_id: row.try_get(2)?,
                    is_parent: row.try_get(3)?,
                })
            })
            .fetch_all(&*self.pool)
            .await?;

        Ok(results)
    }
}
