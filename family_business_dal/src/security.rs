use bcrypt::{BcryptError, DEFAULT_COST};
use family_business_lib::security::User;
use sqlx::{postgres::PgRow, PgPool, Row};
use std::sync::Arc;

#[derive(Debug)]
pub enum UserCreationError {
    UserAlreadyExist,
    DalError(sqlx::Error),
    BcryptError(BcryptError),
}

impl From<sqlx::Error> for UserCreationError {
    fn from(error: sqlx::Error) -> Self {
        UserCreationError::DalError(error)
    }
}

impl From<BcryptError> for UserCreationError {
    fn from(error: BcryptError) -> Self {
        UserCreationError::BcryptError(error)
    }
}

#[derive(Debug)]
pub enum UserFetchError {
    DalError(sqlx::Error),
    BcryptError(BcryptError),
}

impl From<sqlx::Error> for UserFetchError {
    fn from(error: sqlx::Error) -> Self {
        UserFetchError::DalError(error)
    }
}

impl From<BcryptError> for UserFetchError {
    fn from(error: BcryptError) -> Self {
        UserFetchError::BcryptError(error)
    }
}

pub struct Security {
    pool: Arc<PgPool>,
}

impl Security {
    pub fn new(pool: Arc<PgPool>) -> Self {
        Security { pool }
    }

    pub async fn find_by_id(&self, id: i32) -> Result<Option<User>, UserFetchError> {
        let user = sqlx::query("SELECT id, email FROM users WHERE id = $1")
            .bind(&id)
            .try_map(|row: PgRow| {
                Ok(User {
                    id: row.try_get(0)?,
                    email: row.try_get(1)?,
                })
            })
            .fetch_optional(&*self.pool)
            .await?;

        Ok(user)
    }

    pub async fn find_by_email(&self, email: &str) -> Result<Option<User>, sqlx::Error> {
        let results = sqlx::query("SELECT id, email FROM users WHERE email = $1")
            .bind(&email.to_string())
            .try_map(|row: PgRow| {
                Ok(User {
                    id: row.try_get(0)?,
                    email: row.try_get(1)?,
                })
            })
            .fetch_optional(&*self.pool)
            .await?;

        Ok(results)
    }

    pub async fn create_user(
        &self,
        email: &str,
        password: &str,
    ) -> Result<User, UserCreationError> {
        if self.find_by_email(email).await?.is_some() {
            return Err(UserCreationError::UserAlreadyExist);
        }

        let hashed = bcrypt::hash(password, DEFAULT_COST)?;

        sqlx::query("INSERT INTO users (email, password) VALUES ($1, $2)")
            .bind(&email.to_string())
            .bind(&hashed)
            .execute(&*self.pool)
            .await?;

        Ok(self.find_by_email(email).await?.expect("user created"))
    }

    pub async fn get_user(
        &self,
        email: &str,
        password: &str,
    ) -> Result<Option<User>, UserFetchError> {
        let user = sqlx::query("SELECT id, email, password FROM users WHERE email = $1")
            .bind(&email.to_string())
            .fetch_optional(&*self.pool)
            .await?;

        match user {
            Some(user) => {
                if bcrypt::verify(password, user.get(2))? {
                    Ok(Some(User {
                        id: user.get(0),
                        email: user.get(1),
                    }))
                } else {
                    Ok(None)
                }
            }
            None => Ok(None),
        }
    }
}
