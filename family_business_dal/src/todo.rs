use crate::DalError;
use family_business_lib::todo::{Status, ToDo};
use family_business_lib::FamilyMember;
use sqlx::postgres::PgRow;
use sqlx::{PgPool, Row};
use std::sync::Arc;

pub struct Todo {
    pool: Arc<PgPool>,
}

impl Todo {
    pub fn new(pool: Arc<PgPool>) -> Self {
        Todo { pool }
    }

    pub async fn create_todo(&self, todo: &ToDo) -> Result<(), DalError> {
        sqlx::query(
            "INSERT INTO todos(id, name, description, assignee_id, due_date, status) \
                VALUES($1, $2, $3, $4, $5, $6)",
        )
        .bind(&todo.id)
        .bind(&todo.name)
        .bind(&todo.description)
        .bind(&todo.assignee.clone().map(|fm| fm.id))
        .bind(&todo.due_date)
        .bind(&todo.status)
        .execute(&*self.pool)
        .await?;

        Ok(())
    }

    pub async fn get_list(&self, status: &Status) -> Result<Vec<ToDo>, DalError> {
        let rows = sqlx::query(
            "SELECT t.id, t.name, t.description, t.assignee_id, t.due_date, t.status, fm.id, fm.name, fm.user_id, fm.is_parent \
                FROM todos t \
                LEFT JOIN family_members fm ON fm.id = t.assignee_id \
                WHERE status = $1"
        )
        .bind(&status)
        .try_map(|row: PgRow| {
            Ok(ToDo {
                id: row.try_get(0)?,
                name: row.try_get(1)?,
                description: row.try_get(2)?,
                assignee: match row.try_get::<Option<i32>,usize>(3)? {
                    Some(_) => Some(FamilyMember {
                        id: row.try_get(6)?,
                        user_id: row.try_get(7)?,
                        name: row.try_get(8)?,
                        is_parent: row.try_get(9)?
                    }),
                    None => None,
                },
                due_date: row.try_get(4)?,
                status: row.try_get(5)?,
            })
        })
        .fetch_all(&*self.pool)
        .await?;

        Ok(rows)
    }

    pub async fn save_todo(&self, todo: &ToDo) -> Result<(), DalError> {
        sqlx::query(
            "UPDATE todos SET name = $1, description = $2, assignee_id = $3, due_date = $4, status = $5 WHERE id = $6"
        )
        .bind(&todo.name)
        .bind(&todo.description)
        .bind(&todo.assignee.clone().map(|fm| fm.id))
        .bind(&todo.due_date)
        .bind(&todo.status)
        .bind(&todo.id)
        .execute(&*self.pool)
        .await?;

        Ok(())
    }

    pub async fn get(&self, id: uuid::Uuid) -> Result<Option<ToDo>, DalError> {
        let todo = sqlx::query(
            "SELECT t.id, t.name, t.description, t.assignee_id, t.due_date, t.status, fm.id, fm.name, fm.user_id, fm.is_parent \
                FROM todos t \
                LEFT JOIN family_members fm ON fm.id = t.assignee_id \
                WHERE status = $1"
        )
        .bind(&id)
        .try_map(|row: PgRow| {
            Ok(ToDo {
                id: row.try_get(0)?,
                name: row.try_get(1)?,
                description: row.try_get(2)?,
                assignee: match row.try_get::<Option<i32>,usize>(3)? {
                    Some(_) => Some(FamilyMember {
                        id: row.try_get(6)?,
                        user_id: row.try_get(7)?,
                        name: row.try_get(8)?,
                        is_parent: row.try_get(9)?
                    }),
                    None => None,
                },
                due_date: row.try_get(4)?,
                status: row.try_get(5)?,
            })
        })
        .fetch_optional(&*self.pool)
        .await?;

        Ok(todo)
    }
}
