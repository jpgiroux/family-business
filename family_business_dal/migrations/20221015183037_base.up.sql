-- Add up migration script here

CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    email TEXT NOT NULL,
    password TEXT NOT NULL,
    UNIQUE(email)
);

create table IF NOT EXISTS family_members (
	ID SERIAL primary key,
	name TEXT not null,
	is_parent boolean not null,
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TYPE payee AS ENUM ('ALL', 'KIDS');

create table IF NOT EXISTS bills (
    id SERIAL PRIMARY KEY,
    payer_id INTEGER NOT NULL,
    amount REAL NOT NULL,
    payee payee NOT NULL,
    ratios TEXT NOT NULL,
    description TEXT,
    date DATE,
    resolve_date DATE,
    FOREIGN KEY (payer_id) REFERENCES family_members(id)
);

CREATE TYPE todos_status AS ENUM ('OPEN', 'CLOSE');

CREATE TABLE IF NOT EXISTS todos (
    id TEXT PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    assignee_id INT,
    due_date DATE,
    status todos_status,
    FOREIGN KEY (assignee_id) REFERENCES family_members(id)
)