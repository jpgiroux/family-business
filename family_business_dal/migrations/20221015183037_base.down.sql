-- Add down migration script here

DROP TABLE todos;

DROP TYPE todos_status;

DROP TABLE bills;

DROP TYPE payee;

DROP TABLE family_members;

DROP TABLE users;