use crate::bills::BillSaveError::BadRequest;
use crate::security::User;
use family_business_dal::{Dal, DalError};
use family_business_lib::bills::ratio::RatioError;
use family_business_lib::bills::{
    Bill, BillResolvedQueryResult, BillSortExpression, BillsResolution, BillsResolutionError,
    NewBill,
};
use family_business_lib::query::Pagination;
use rocket::fairing::AdHoc;
use rocket::response::status::Created;
use rocket::response::Debug;
use rocket::serde::json::Json;
use rocket::Responder;
use rocket::State;

type Result<T, E = Debug<DalError>> = std::result::Result<T, E>;

pub fn stage() -> AdHoc {
    AdHoc::on_ignite("Bills stage", |rocket| async {
        rocket.mount(
            "/bills",
            routes![
                add_bill,
                get_bills,
                delete_bill,
                get_bill,
                put_bill,
                get_bills_resolved,
                resolve_bills,
            ],
        )
    })
}

#[derive(Debug, Clone, Responder)]
pub enum BillSaveError {
    #[response(status = 400, content_type = "json")]
    BadRequest(String),
    #[response(status = 500, content_type = "json")]
    InternalError(String),
}

impl From<DalError> for BillSaveError {
    fn from(e: DalError) -> Self {
        BillSaveError::InternalError(e.to_string())
    }
}

impl From<RatioError> for BillSaveError {
    fn from(_: RatioError) -> Self {
        BillSaveError::BadRequest("Ratios_invalid".to_string())
    }
}

impl From<BillsResolutionError> for BillSaveError {
    fn from(_: BillsResolutionError) -> Self {
        BillSaveError::BadRequest("Bills not valids".to_string())
    }
}

#[post("/", data = "<new_bill>")]
pub(crate) async fn add_bill(
    dal: &State<Dal>,
    _user: User,
    new_bill: Json<NewBill>,
) -> Result<Created<Json<Bill>>, BillSaveError> {
    family_business_lib::bills::ratio::validate(&new_bill.ratios)?;

    let created = dal.bills.create_bill(&new_bill).await?;

    Ok(Created::new("/bills").body(Json(created)))
}

#[get("/")]
pub(crate) async fn get_bills(dal: &State<Dal>, _user: User) -> Result<Json<Vec<Bill>>> {
    let bills: Vec<Bill> = dal.bills.get_bills().await?;

    Ok(Json(bills))
}

#[delete("/<id>")]
pub(crate) async fn delete_bill(dal: &State<Dal>, _user: User, id: i32) -> Result<()> {
    Ok(dal.bills.delete_bill(id).await?)
}

#[get("/<id>")]
pub(crate) async fn get_bill(dal: &State<Dal>, _user: User, id: i32) -> Result<Option<Json<Bill>>> {
    let result = dal.bills.get_bill(id).await?;

    match result {
        Some(bill) => Ok(Some(Json(bill))),
        None => Ok(None),
    }
}

#[put("/<id>", data = "<bill>")]
pub(crate) async fn put_bill(
    dal: &State<Dal>,
    _user: User,
    id: i32,
    bill: Json<Bill>,
) -> Result<Json<Bill>, BillSaveError> {
    if id != bill.id {
        return Err(BadRequest("id does not match bill id".to_string()));
    }

    family_business_lib::bills::ratio::validate(&bill.ratios)?;

    let edited = dal.bills.edit_bill(&bill).await?;

    Ok(Json(edited))
}

#[post("/resolution", data = "<bill_resolution>")]
pub(crate) async fn resolve_bills(
    dal: &State<Dal>,
    _user: User,
    bill_resolution: Json<BillsResolution>,
) -> Result<(), BillSaveError> {
    let current_unresolved: Vec<Bill> = dal.bills.get_bills().await?;

    family_business_lib::bills::validate(&bill_resolution, &current_unresolved)?;

    dal.bills.resolve_bills(&bill_resolution).await?;

    Ok(())
}

#[get("/resolved?<pagination>&<sort>")]
pub(crate) async fn get_bills_resolved(
    dal: &State<Dal>,
    _user: User,
    pagination: Option<Pagination>,
    sort: Option<BillSortExpression>,
) -> Result<Json<BillResolvedQueryResult>> {
    let pagination = pagination.unwrap_or_else(Pagination::default);
    let sort = sort.unwrap_or_else(BillSortExpression::default);

    let resolved_query_result = dal.bills.get_bills_resolved(pagination, sort).await?;

    Ok(Json(resolved_query_result))
}
