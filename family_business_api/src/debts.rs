use crate::security::User;
use family_business_dal::{Dal, DalError};
use family_business_lib::{
    bills::{debt::Debt, Bill},
    FamilyMember,
};
use rocket::fairing::AdHoc;
use rocket::response::Debug;
use rocket::serde::json::Json;
use rocket::State;

type Result<T, E = Debug<DalError>> = std::result::Result<T, E>;

pub fn stage() -> AdHoc {
    AdHoc::on_ignite("Debts stage", |rocket| async {
        rocket.mount("/debts", routes![get_debts])
    })
}

#[get("/")]
pub(crate) async fn get_debts(dal: &State<Dal>, _user: User) -> Result<Json<Vec<Debt>>> {
    let bills: Vec<Bill> = dal.bills.get_bills().await?;

    let family: Vec<FamilyMember> = dal.get_members().await?;

    let debts = family_business_lib::bills::debt::calculate_debt(&family, &bills);

    Ok(Json(debts))
}
