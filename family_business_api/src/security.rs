use chrono::Utc;
use config::Config;
use family_business_dal::security::UserFetchError;
use family_business_dal::Dal;
use family_business_lib::security::User as UserDto;
use jsonwebtokens as jwt;
use jsonwebtokens::encode;
use jsonwebtokens::error::Error as JwtError;
use jwt::{Algorithm, AlgorithmID, Verifier};
use rocket::fairing::AdHoc;
use rocket::http::Status;
use rocket::request::{FromRequest, Outcome};
use rocket::serde::json::Json;
use rocket::Responder;
use rocket::{Request, State};
use serde::{Deserialize, Serialize};
use serde_json::json;
use serde_json::value::Value;
use std::ops::Add;

#[derive(Debug, Deserialize)]
struct TokenConfig {
    issuer: String,
    audience: String,
    secret_key: String,
    token_exp: i64,
}

pub fn stage() -> AdHoc {
    let token_config = Config::builder()
        .add_source(config::File::with_name("jwt").required(false))
        .add_source(config::Environment::with_prefix("jwt"))
        .build()
        .expect("config builded")
        .try_deserialize::<TokenConfig>()
        .expect("token config deserialized");

    AdHoc::on_ignite("Security stage", |rocket| async {
        rocket
            .manage(token_config)
            .mount("/users_sessions", routes![post_users_sessions])
    })
}

#[derive(Debug, Deserialize, PartialEq, Clone)]
struct LoginRequest {
    pub email: String,
    pub password: String,
}

#[derive(Debug, Serialize, PartialEq, Clone)]
struct LoginResponse {
    pub token: String,
}

#[derive(Debug, Clone, Responder)]
pub enum LoginError {
    #[response(status = 401, content_type = "json")]
    TokenInvalid(String),
    #[response(status = 404, content_type = "json")]
    UserNotFound(String),
    #[response(status = 500, content_type = "json")]
    InternalError(String),
}

impl From<JwtError> for LoginError {
    fn from(error: JwtError) -> Self {
        LoginError::InternalError(error.to_string())
    }
}

impl From<serde_json::Error> for LoginError {
    fn from(error: serde_json::Error) -> Self {
        LoginError::InternalError(error.to_string())
    }
}

impl From<UserFetchError> for LoginError {
    fn from(error: UserFetchError) -> Self {
        match error {
            UserFetchError::BcryptError(e) => LoginError::InternalError(e.to_string()),
            UserFetchError::DalError(e) => LoginError::InternalError(e.to_string()),
        }
    }
}

#[post("/", data = "<login_request>")]
async fn post_users_sessions(
    dal: &State<Dal>,
    login_request: Json<LoginRequest>,
    token_config: &State<TokenConfig>,
) -> Result<Json<LoginResponse>, LoginError> {
    let user = dal
        .security
        .get_user(&login_request.email, &login_request.password)
        .await?;

    match user {
        Some(user) => Ok(Json(LoginResponse {
            token: build_token(user.id, token_config.inner())?,
        })),
        None => Err(LoginError::UserNotFound("Not found".to_string())),
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct TokenClaim {
    pub iss: String,
    pub aud: String,
    pub sub: String,
    pub exp: usize,
}

impl TokenClaim {
    pub fn new(sub_id: i32, iss: &str, aud: &str, exp: i64) -> TokenClaim {
        let expiration = Utc::now().add(chrono::Duration::seconds(exp)).timestamp();

        TokenClaim {
            iss: iss.to_string(),
            aud: aud.to_string(),
            sub: sub_id.to_string(),
            exp: expiration as usize,
        }
    }
}

fn build_token(user_id: i32, token_config: &TokenConfig) -> Result<String, JwtError> {
    let alg = Algorithm::new_hmac_b64(AlgorithmID::HS256, &token_config.secret_key)
        .expect("Secret key / algo error");
    let header = json!({ "alg": alg.name() });
    let claims = TokenClaim::new(
        user_id,
        &token_config.issuer,
        &token_config.audience,
        token_config.token_exp,
    );
    encode(&header, &claims, &alg)
}

fn validate_token(token_str: &str, token_config: &TokenConfig) -> Result<TokenClaim, LoginError> {
    let alg = Algorithm::new_hmac_b64(AlgorithmID::HS256, &token_config.secret_key)?;
    let verifier = Verifier::create()
        .issuer(&token_config.issuer)
        .audience(&token_config.audience)
        .build()?;
    let claims: Value = verifier.verify(&token_str, &alg)?;
    Ok(serde_json::from_value(claims)?)
}

#[derive(Debug, Clone)]
pub struct User {
    pub user: UserDto,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for User {
    type Error = LoginError;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let dal = req.guard::<&State<Dal>>().await;
        let token_config = req.guard::<&State<TokenConfig>>().await;

        match req.headers().get_one("Authorization") {
            None => Outcome::Forward(()),
            Some(bearer_token) => match token_config {
                Outcome::Success(token) => match validate_token(&bearer_token[7..], token) {
                    Err(_) => Outcome::Failure((
                        Status::Unauthorized,
                        LoginError::TokenInvalid("token invalid".to_string()),
                    )),
                    Ok(token) => {
                        let user_id = token.sub.parse::<i32>().expect("user id to be int");
                        match dal {
                            Outcome::Success(dal) => match dal.security.find_by_id(user_id).await {
                                Err(_) => Outcome::Failure((
                                    Status::InternalServerError,
                                    LoginError::InternalError("db error".to_string()),
                                )),
                                Ok(user_opt) => match user_opt {
                                    None => Outcome::Failure((
                                        Status::NotFound,
                                        LoginError::UserNotFound("user not found".to_string()),
                                    )),
                                    Some(user) => Outcome::Success(User { user }),
                                },
                            },
                            _ => Outcome::Failure((
                                Status::InternalServerError,
                                LoginError::InternalError("db guard fail".to_string()),
                            )),
                        }
                    }
                },
                _ => Outcome::Failure((
                    Status::InternalServerError,
                    LoginError::InternalError("token config not found".to_string()),
                )),
            },
        }
    }
}
