use rocket::fairing::AdHoc;

pub fn stage() -> AdHoc {
    AdHoc::try_on_ignite("DAL Setup", |rocket| async {
        let db_url = rocket.figment().find_value("databases.db.url");

        match db_url {
            Ok(db_url) => {
                let dal = family_business_dal::Dal::new(db_url.as_str().unwrap()).await;

                match dal {
                    Ok(dal) => Ok(rocket.manage(dal)),
                    Err(e) => {
                        error!("Failed to init DAL: {}", e);
                        Err(rocket)
                    }
                }
            }
            Err(e) => {
                error!("Failed to load db url: {}", e);
                Err(rocket)
            }
        }
    })
}
