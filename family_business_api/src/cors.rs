use rocket::fairing::AdHoc;
use rocket::http::{Header, Method, Status};

pub fn stage() -> AdHoc {
    AdHoc::on_response("Cors on response", |req, response| {
        Box::pin(async move {
            response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
            response.set_header(Header::new(
                "Access-Control-Allow-Methods",
                "POST, GET, PUT, DELETE, PATCH, OPTIONS",
            ));

            response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
            response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));

            if req.method() == Method::Options {
                response.set_status(Status::Ok)
            }
        })
    })
}
