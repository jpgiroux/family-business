use crate::security::User;
use family_business_dal::{Dal, DalError};
use family_business_lib::todo::{AddToDoRequest, EditToDoRequest, ToDo, ToDoError};
use family_business_lib::FamilyMember;
use rocket::fairing::AdHoc;
use rocket::response::status::Created;
use rocket::serde::json::Json;
use rocket::Responder;
use rocket::State;

pub fn stage() -> AdHoc {
    AdHoc::on_ignite("ToDo stage", |rocket| async {
        rocket.mount(
            "/todo",
            routes![
                add_todo,
                get_opened_todos,
                get_todo,
                edit_todo,
                close_todo,
                get_closed_todos
            ],
        )
    })
}

#[derive(Debug, Clone, Responder)]
pub enum ToDoApiError {
    #[response(status = 400, content_type = "json")]
    BadRequest(String),
    #[response(status = 404, content_type = "json")]
    NotFound(String),
    #[response(status = 500, content_type = "json")]
    InternalError(String),
}

impl From<DalError> for ToDoApiError {
    fn from(error: DalError) -> Self {
        ToDoApiError::InternalError(error.to_string())
    }
}

impl From<ToDoError> for ToDoApiError {
    fn from(error: ToDoError) -> Self {
        ToDoApiError::BadRequest(error.to_string())
    }
}

impl From<uuid::Error> for ToDoApiError {
    fn from(error: uuid::Error) -> Self {
        ToDoApiError::BadRequest(error.to_string())
    }
}

#[post("/", data = "<new_todo>")]
pub(crate) async fn add_todo(
    dal: &State<Dal>,
    _user: User,
    new_todo: Json<AddToDoRequest>,
) -> Result<Created<Json<ToDo>>, ToDoApiError> {
    let family: Vec<FamilyMember> = dal.get_members().await?;

    let to_do = family_business_lib::todo::ToDo::from_add_request(&new_todo, &family)?;
    let returned = to_do.clone();

    dal.todo.create_todo(&to_do).await?;

    Ok(Created::new("/todo").body(Json(returned)))
}

#[get("/")]
pub(crate) async fn get_opened_todos(
    dal: &State<Dal>,
    _user: User,
) -> Result<Json<Vec<ToDo>>, ToDoApiError> {
    let todos: Vec<ToDo> = dal
        .todo
        .get_list(&family_business_lib::todo::Status::Open)
        .await?;

    Ok(Json(todos))
}

#[get("/<id>")]
pub(crate) async fn get_todo(
    dal: &State<Dal>,
    _user: User,
    id: &str,
) -> Result<Option<Json<ToDo>>, ToDoApiError> {
    let id = uuid::Uuid::parse_str(id)?;
    let todo_opt: Option<ToDo> = dal.todo.get(id).await?;

    match todo_opt {
        Some(todo) => Ok(Some(Json(todo))),
        None => Ok(None),
    }
}

#[put("/", data = "<edit_request>")]
pub(crate) async fn edit_todo(
    dal: &State<Dal>,
    _user: User,
    edit_request: Json<EditToDoRequest>,
) -> Result<Json<ToDo>, ToDoApiError> {
    let family: Vec<FamilyMember> = dal.get_members().await?;

    let to_do_id = edit_request.id;
    let opt_todo = dal.todo.get(to_do_id).await?;

    if let Some(mut todo) = opt_todo {
        todo.apply_edit_request(&edit_request, &family)?;
        let copy_updated = todo.clone();
        dal.todo.save_todo(&todo).await?;
        Ok(Json(copy_updated))
    } else {
        Err(ToDoApiError::NotFound(format!(
            "No todo with id {}",
            &edit_request.id
        )))
    }
}

#[post("/closed/<id>")]
pub(crate) async fn close_todo(
    dal: &State<Dal>,
    _user: User,
    id: &str,
) -> Result<Json<ToDo>, ToDoApiError> {
    let id = uuid::Uuid::parse_str(id)?;
    let opt_todo = dal.todo.get(id).await?;

    match opt_todo {
        Some(mut todo) => {
            todo.close();
            let copy = todo.clone();
            dal.todo.save_todo(&todo).await?;
            Ok(Json(copy))
        }
        None => Err(ToDoApiError::NotFound(format!("No todo with id {}", id))),
    }
}

#[get("/closed")]
pub(crate) async fn get_closed_todos(
    dal: &State<Dal>,
    _user: User,
) -> Result<Json<Vec<ToDo>>, ToDoApiError> {
    let todos: Vec<ToDo> = dal
        .todo
        .get_list(&family_business_lib::todo::Status::Close)
        .await?;

    Ok(Json(todos))
}
