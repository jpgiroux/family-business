#[macro_use]
extern crate rocket;

mod bills;
mod cors;
mod dal;
mod debts;
mod family;
mod security;
mod todo;

#[get("/")]
async fn index() -> &'static str {
    "Welcome to the fop API v0.1.2"
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        // .attach(DbConn::fairing())
        .attach(dal::stage())
        .attach(cors::stage())
        .attach(security::stage())
        .attach(family::stage())
        .attach(bills::stage())
        .attach(debts::stage())
        .attach(todo::stage())
        .mount("/", routes![index])
}
