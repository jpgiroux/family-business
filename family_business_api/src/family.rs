use crate::security::User;
use family_business_dal::{Dal, DalError};
use family_business_lib::{FamilyMember, NewFamilyMember};
use rocket::fairing::AdHoc;
use rocket::response::status::Created;
use rocket::response::Debug;
use rocket::serde::json::Json;
use rocket::State;

type Result<T, E = Debug<DalError>> = std::result::Result<T, E>;

pub fn stage() -> AdHoc {
    AdHoc::on_ignite("Family stage", |rocket| async {
        rocket.mount("/family", routes![add_member, get_members, get_member])
    })
}

#[post("/members", data = "<new_member>")]
pub(crate) async fn add_member(
    dal: &State<Dal>,
    _user: User,
    new_member: Json<NewFamilyMember>,
) -> Result<Created<Json<NewFamilyMember>>> {
    let val = new_member.clone();

    dal.create_member(&new_member).await?;

    Ok(Created::new("/members").body(val))
}

#[get("/members")]
pub(crate) async fn get_members(dal: &State<Dal>, _user: User) -> Result<Json<Vec<FamilyMember>>> {
    let members: Vec<FamilyMember> = dal.get_members().await?;
    Ok(Json(members))
}

#[get("/members/<id>")]
pub(crate) async fn get_member(
    dal: &State<Dal>,
    _user: User,
    id: i32,
) -> Result<Option<Json<FamilyMember>>> {
    let result = dal.get_member(id).await?;

    match result {
        Some(member) => Ok(Some(Json(member))),
        None => Ok(None),
    }
}
