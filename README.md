# FOP

A family intranet where you managed bills, todo and (soon) more.

### Docker

Build the api and cli
```
docker build -t fop_cargo .
```

To run the api, you must provide these env variables
```
JWT_ISSUER=...
JWT_AUDIENCE=...
JWT_SECRET_KEY=...
JWT_TOKEN_EXP=...
ROCKET_DATABASES={db={url=\".....\"}}
```

To run the cli, you must provide this env variable
```
FOP__DB_URL=...
```